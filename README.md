# WBPlay Performance Tool

A command-line tool to automate deployment and management of a JMeter cluster in AWS.

## About

When doing any kind of performance testing, often times, the bottleneck is on the client-side.  A single machine can only do so much before running out of local resources, i.e.memory, CPU, IO, etc.

One way to address this is to distribute the load across many machines.  Fortunately, JMeter does have pretty good support for this.  It can be configured into a master-slave cluster.  The tester would only need to interact with the master node, loading test scripts and executing them. The master node automatically distributes and executes the test across multiple slave nodes.  Each slave node reports results data back to the master node which generates aggregate reports.

This tool automates the tedious steps needed to setup and manage a JMeter cluster in AWS.

## Typical Use Case

Performance engineer (PE) writes tests on local workstation.  The test is sophisticated and uses several beanshell samplers that depends on custom jar files.  It also uses test data in csv files.

After increasing the thread (user) count to a certain point, the PE sees that the CPU on the local workstation spikes up to 100% clearly indicating that the test client has become the bottleneck.  It's time to distribute the test load across multiple machines.

PE wants to start out small, she spins up 2 slave nodes. (Note: that the JMeter master node only does orchestration, it is the slave nodes that actually executes the tests.)  She executes the following command:

```
wbplay-perftool "jdoe-eventstore" up 2
```

This simple commmand spins up 4 AWS EC2 instances and installs/configures JMeter on them:
* jdoe-eventstore-jmeter-master
* jdoe-eventstore-jmeter-slave-1
* jdoe-eventstore-jmeter-slave-2
* jdoe-eventstore-jmeter-monitor

Of course, PE does not need to be concerned with these details.  Once completed, the command prints the master node RDP and InfluxDB/Grapfana connection information.  

PE updates the test by adding a backend listener to the test and configuring it with the monitor connection information.

Next, PE sends all test dependencies with the command:

```
wbplay-perftool "jdoe-eventstore" transfer_files d:\test_files\*.*
```

PE executes the test by running the following command:

```
wbplay-perftool "jdoe-eventstore" run d:\test_files\eventstore.jmx Deventstore.host=43.245.32.245
```

Alternatively, PE can open a RDP client and connect, or simply execute the following command:

```
wbplay-perftool "jdoe-eventstore" connect
```

This will automatically launch the RDP client and connect to the master node.  There's a convenience launcher for JMeter on the desktop.

PE has a problem though.  The test project is on the local machine.  Also, JMeter does not automatically pushes dependencies and external files to the slave nodes.  Her test project (eventstore.jmx), dependent jars (\*.jar), and test data (\*.csv) are all on her local workstation.  The evenstore.jmx file needs to be on the master node so she can open it in JMeter.  The jars and test data files needs to be on every slave nodes since JMeter will need them to run the tests.  

Fortunately, that can easily be remedied using the following command:

```
wbplay-perftool "jdoe-eventstore" transfer_files "d:\local\path\to\test\files\*.*"
```

This is the simpliest form of the **tranfer_files** command.  It will copy all files in a directory (assuming all files can be put into a single directory) to the appropiate JMeter directory of each nodes (including the master and all slaves.) This command has many more options, see below for details.

PE can now open "eventstore.jmx" project file (in the JMeter's bin directory) on the master node via RDP.

However, there is one more problem.  When jar files are deployed, JMeter needs to be restarted in order to pick them up.  There's a command for that too:

```
wbplay-perftool "jdoe-eventstore" restart
```

That's it, PE can now execute the tests across all the slave nodes.  Once PE is done with the environment, it should be properly destroyed (AWS is expensive!)  There's a command for that as well:

```
wbplay-perftool "jdoe-eventstore" destroy
````

## How to Install

Python 2.7 + Pip is required.  Simply execute the following in a command line (Bash or Windows CMD should work):

```
$ pip install git+https://github.turbine.com/WBNet/wbplay-perftool
```

## How to Uninstall

If wbplay-perftool package is changed, you want to uninstall the tool and reinstall it on your machine before you create a new jmeter perofrmance test environment. The way to uninstall the tool is simply to execute the following command

```
pip uninstall wbplay-perftool
```

### Setup Boto AWS Credentials
This tool requires access to AWS and reads credentials from a boto config file.  If you haven't already set this up, uou will need to generate the an access key and add it to the config file.

#### Generate Access Key
1. Sign into AWS console with your IAM credentials
2. Click on the account menu at the top-right and select "Security Credentials".
3. Click on "Users" and find your account in the list.
4. Click on your account to bring up the details page.
5. On the details page, scroll down and click on the "Manage Access Keys" button.
6. Click on the "Create Access Key" button.
7. Click on Show User Security Credentials and make note of the Access Key ID and Secret Access Key.

### Create/Update Default Boto Config

1. Create an empty text file in your account's directory, typically C:\Users\<username>\boto.config where <username> is your Windows username. Or on Linux-based systems, at ~/.boto.
2. Add the following text to the file:
```
[Credentials]
aws_access_key_id = <your_access_key_here>
aws_secret_access_key = <your_secret_key_here>
```
3. On Windows, create a user environment variable named BOTO_CONFIG and set it to the config file (i.e. C:\Users\<username>\boto.config)

### Setup SSH Private Key configuration
Some of the perftool commands require SSH access to AWS. The authentication for this is done securely via a private SSH key.
You can either set the [definition][environment][key_file_path] property in the config file manually or use the privatekey command from the CLI. For example:
```
python wbplay_perftool.py "jgarland-test" privatekey d:\aws\swarm-us-east.pem
```

### Create and Using Multiple Boto Profiles
1.  See the previous section to create the inital boto config file.
2.  Add the following text to the file where your_profile_name1 is the name of your profile:
```
[profile your_profile_name1]
aws_access_key_id = <your_access_key_here1>
aws_secret_access_key = <your_secret_key_here1>

[profile your_profile_name2]
aws_access_key_id = <your_access_key_here2>
aws_secret_access_key = <your_secret_key_here2>
```
3. For the following commands: [up, destroy, restart, transfer_files, connect, run], add the optional --profile option with the profile you want to use.

For example:
```
wbplay-perftool "my-jmeter-cluster" run "~/test_files/eventstore-test.jmx" --profile your_profile_name2
```

## Executing Commands

Commands are executed in the following format:

```
wbplay-perftool --config <optional_config> <cluster-name> <command> <command parameters>
```

where:

**cluster-name** is the environment prefix.  It should uniquely identify your cluster, perhaps \<username\>-\<project\> (jdoe-eventstore).

**command** is one of the following:

* up
* destroy
* restart
* transfer_files
* connect
* run
 
Environment configuration resides in default.config, this includes AMI used, AWS API keys, etc..  You can optionally specify a **custom config file** with --config_file or simply modify the default.cfg.

### "privatekey" Command

Some of the perftool commands require SSH access to AWS. The authentication for this is done securely via a private SSH key.
You use this command to set the location of your SSH key.

The parameters for "privatekey" are:
* cluster name
* private key location

Example:
```
python wbplay_perftool.py "jgarland-test" privatekey d:\aws\swarm-us-east.pem
```

### "up" Command

The "up" command spins up the JMeter cluster and wire them up.  When completed, you'll have a working JMeter cluster ready to run your test in AWS.  This includes:

* Master node - A Linux-based Ubuntu desktop node that you can RDP into and run JMeter.
* Slave JMeter nodes - X numbers of JMeter slave nodes, all setup with the master.

The parameters for "up" are:
* Number of slave nodes to spawn.  Minimum of 1.
* Optional --monitor-host: The monitor instance to use instead of spawning a new monitor instance.
* Optional: "--profile" -  The boto profile to use.
 
NOTE: When "up" command completes, it will display the RDP address of the master and monitor nodes. At which point, you can connect and use JMeter and/or review metrics.  Another option is to use the **connect** command which starts RDP and connect to the instance.

Example:
```
wbplay-perftool "jdoe-eventstore" up 5
```

### "destroy" Command

The "destroy" command completely destroys the cluster.  It will terminate all instances in AWS.

The parameters for "destroy" are:
* Optional: "--profile" -  The boto profile to use.

Example:
```
wbplay-perftool "jdoe-eventstore" destroy
```

### "restart" Command

The "restart" command recycles all the JMeter server instances on each slave node. This is necessary when new JARs are pushed out, or when JMeter ends up in a wonky state.  Note, it only recycles the JMeter service, not the entire node.

The parameters for "destroy" are:
* Optional: "--profile" -  The boto profile to use.

Example:
```
wbplay-perftool "jdoe-eventstore" restart
```

### "transfer_files" Command

The "transfer_files" command pushes files from a local directory to the remote AWS nodes.  Basic wildcard matching is supported.  For example, "*.jar".

The simpliest use-case is that you have a bunch of test files in a directory that you want to transfer to every node and let the tool decide on where to put the file on each node.

Example:
```
wbplay-perftool "jdoe-eventstore" transfer_files "d:\my_test_files\*.*"
```

The tool will automatically push files from the source directory to all nodes (including the master) and put them according to the following rules:
* If the file ends in "jar", it will put them in the apache-jmeter/lib/ext directory.
* All other filetypes are put in the apache-jmeter/bin directory.
 
It is also possible to specify the optional destination directory.  For example, the following command will transfer the files to the home directory.
```
wbplay-perftool "jdoe-eventstore" transfer_files "d:\my_test_files\*.*" "~/"
```

If you want to transfer files to only a particular node, use "--node_filter <node_identifer>".  <node_identifer> must be one of the following:
* m - master node
* s* - All slave nodes
* sX - Slave X node where X is an integer between 1 and node size inclusive.

For example, the following command only transfers to the master node.
```
wbplay-perftool "jdoe-eventstore" transfer_files "d:\my_test_files\*.*" --node_filter m
```

Another popular use-case is to remap files from a local directory to the same filepath on each slave node.  For example:
* d:\test\_data\user1.csv -> slave\_node\_1: /test_data/user.csv
* d:\test\_data\user2.csv -> slave\_node\_2: /test_data/user.csv
* d:\test\_data\user3.csv -> slave\_node\_3: /test_data/user.csv

This is necessary if you have user data that needs to be used only once by each test thread. If you push the same test data to all slave nodes, they would overlap.

The "--remap <filename>" option will allow you to do this.  For example,
```
wbplay-perftool "jdoe-eventstore" transfer_files "d:\test_data\*.csv" "/test_data" --remap "user.csv"
```

The number of nodes should match the number of files in the source directory.  The algorithm used to determine the mapping is purely based on the natural order of the files in the directory.  The first file in the directory maps to first slave node, the second file maps to the second slave node, and so on.

It takes the following parameters:
* SOURCE - Directory or file to transfer.  Basic wildcard is supported.
* Optional: DESTINATION - Absolute path is supported as well as relative path to the JMeter directory.  If omitted, jars will be pushed to the lib/ext/ directory, while all other filetypes would be push into the JMeter bin directory.
* Optional: "-node\_filter m/s\*/sN" - Only copy files to a particular node.  For example, "-node\_filter m" will only push files to the master.  "-node s1" will only push files to slave 1.  Default behavior is to push to all nodes.
* Optional: "-remap" - Map files to a single file on each node.
* Optional: "--profile" -  The boto profile to use.

```
wbplay-perftool push_files "~/dev/performance/eventstore_client/bin/*.jar"
```

### "connect" Command

The "connect" command is a convenience way to starts an RDP session with the master node.  May only work in Windows.

It takes no parameters.

### "run" Command

The "run" command executes a test remotely.


It takes the following parameters:
* TEST - The local (where the tool is executed from) path to the test file.
* Optional: "--logfile-name" - The name of the result csv file that JMeter will write to for the run.
* Optional: "--profile" -  The boto profile to use.
* Optional: JMETER_PROPERTIES - Limitless sets of key/value pairs for JMeter Properties.
* 
```
wbplay-perftool "your jmeter cluster name here" run "~/test_files/eventstore-test.jmx" prop1=value1 prop2=value2
```

It takes no parameters.

## Preparing and Running Tests
- Copy the test and all dependencies into a new folder. For event-store.jmx, it depended on two jars (event-store.jar, postgresql-9.2-1002.jdbc4.jar) I created a new directory called "event-store-test"
- For the local run, copy the dependent jars (event-store.jar, postgresql-9.2-1002.jdbc4.jar, and all jars in this repo's lib directory to the JMeter's lib\ext.
- Start JMeter and open the test.
- Add a Setup Thread Group if one does not exist, then add a Java Request.  Change the classname to org.turbine.wbplay.annotationclient.AnnotationJavaSamplerClient, use the following for Configuration values:
 * Host: ${influxdb.host}
 * Port: ${influxdb.port}
 * Username: ${influxdb.username}
 * Password: ${influxdb.password}
 * Title: "Start of PostgreSQL Test"
 * Tags: "eventsourcing eventstore postgreSQL performance"
 * Text: "10 concurrent users, 1000 iteration"
- Add a Teardown Thread Group if one does not exist, then add a Java Request.  Change the classname to org.turbine.wbplay.annotationclient.AnnotationJavaSamplerClient, use the following for Configuration values:
 * Host: ${influxdb.host}
 * Port: ${influxdb.port}
 * Username: ${influxdb.username}
 * Password: ${influxdb.password}
 * Title: "End of PostgreSQL Test"
 * Tags: "eventsourcing eventstore postgreSQL performance"
 * Text: "10 concurrent users, 1000 iteration"
- Add a Backend Listener, change Backend implementation to: org.turbine.wbplay.jmeter.visualizers.backend.graphite.GraphiteBackendListenderClient and use the following parameters:
 * graphiteMetricsSender: org.turbine.wbplay.jmeter.visualizers.backend.graphite.TextGraphiteMetricsSender
 * graphiteHost: ${influxdb.host}
 * graphitePort: ${influxdb.graphite_port}
 * rootMetricsPrefix: jmeter.postgreSQL.
 * summaryOnly: False
 * samplersList: write.single;
 * percentiles: 90;95;99

- Use an existing or spin up your environment with: wbplay-perftool "event-store-test" up 2

## Environment Configuration File

The environment file contains AWS settings that can be tweaked but are uncommon changes that does not warrant having them as arguments or options on the command-line.  It is JSON-encoded.

The settings are:
* title - The game title that is being tested by the JMeter cluster.  It can also be non-title specific such as an internal effort to test a particular product.
* templates/slave/instance_type - The AWS instance type that should be used for the slave nodes.
* templates/slave/ami_id - The AMI id that should be used for the slave nodes.
* templates/master/instance_type - The AWS instance type that should be used for the master node.
* templates/master/ami_id - The AMI id that should be used for the master node. 
* environment/region - The AWS region for the cluster.
* environment/access_key - THE AWS access key
* environment/secret\_access\_key - THE AWS access key secret.
* environment/key\_pair\_name - The AWS SSH key to install for remote access
* environment/security_group - The security group to use for the nodes.
* environment/subnet_id - The subnet id to use for the nodes.
* environment/device_mapping/../size - the disk size of each node.
* environment/ebs_optimized - Whether the noodes should be EBS optimized.

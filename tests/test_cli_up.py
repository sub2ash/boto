import mock
from wbplay_perftool import wbplay_perftool


def test_up_noArguments_helpDisplayed(runner):
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", "up", "--help"])

    # We should have Usage information
    assert 'Usage: cli up [OPTIONS] SLAVE_NODE_COUNT' in result.output
    assert 'The "up" command' in result.output


def test_up_noNodeCount_missingArgumentDisplayed(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'up'])

    print result.output

    # We should have Usage information
    assert 'Usage: cli up [OPTIONS] SLAVE_NODE_COUNT' in result.output
    assert 'Error: Missing argument "slave_node_count"' in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.run_instance')
@mock.patch('wbplay_perftool.instance_controls.configure_jmeter_properties_file')
@mock.patch('wbplay_perftool.instance_controls.configure_collectd')
def test_up_withClusterNameAndNodeCount_clusterCreated(mock_configure_collectd,
                                                       mock_configure_jmeter_properties_file,
                                                       mock_run_instance,
                                                       mock_get_instances_for_cluster,
                                                       runner,
                                                       moto_jmeter_cluster_instances):

    # We need to mock get instances because it will return out moto instances right now
    mock_get_instances_for_cluster.return_value = []
    # We mock run_instance to return each one of our moto instances in sequence
    mock_run_instance.side_effect = moto_jmeter_cluster_instances

    # These two functions will be unit tested separately, so we disable them via mock
    mock_configure_collectd.return_value = None
    mock_configure_jmeter_properties_file.return_value = None

    result = runner.invoke(wbplay_perftool.cli, ['pytest-cluster', 'up', '2'])
    assert result.exception is None
    assert 'Launching EC2 instance' in result.output
    assert 'Connection Info' in result.output


def test_up_withClusterNameAndNodeCountZero_invalidValueError(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'up', '0'])

    assert result.exception
    assert 'Error: Invalid value: Node count must be a positive integer' in result.output


def test_up_clusterExists_clusterExistsError(moto_aws_connection, runner):
    with mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster') as mock_get_all_instances:
        mock_get_all_instances.return_value = ['fakey instance']

        result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'up', '1'])

        assert result.exception
        assert 'JMeter cluster "pytest-cluster" already exists! Please destroy it first.' in result.output


def test_up_profile_doesntExist_clickBadParameterException(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'up', '--profile', 'myprofile', '2'])

    assert result.exception
    assert 'The specified profile "myprofile" does not appear to be set up in your BOTO_CONFIG' in result.output


@mock.patch('wbplay_perftool.wbplay_perftool.get_aws_config')
def test_up_noPrivateKey_ClickUsageError(mock_get_aws_config, moto_aws_connection, runner, config_no_key_file_path):
    # Use the modified config file
    mock_get_aws_config.return_value = config_no_key_file_path

    # Call the command
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'up', '1'])

    # There should be an error regarding the SSH key path
    assert result.exception
    assert 'Error: Your config is missing the SSH key path. Please run the privatekey command to set it'\
           in result.output

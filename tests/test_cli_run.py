from wbplay_perftool import wbplay_perftool
import mock


def test_run_noArguments_missingArgumentsMessage(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", 'run'])

    # We should have Usage information
    assert 'Usage: cli run' in result.output
    assert 'Error: Missing argument "test_file".' in result.output


def test_run_help_helpDisplayed(runner):
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", 'run', '--help'])

    # We should have Usage information
    assert result.exception is None
    assert 'The "run" command transfers your' in result.output


def test_run_testFile_doesNotExist_clickBadParameterException(moto_jmeter_cluster_instances, runner):
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", 'run', r'z:\not_a_valid_file.jmx'])

    assert result.exception
    assert r'Not a valid file: z:\not_a_valid_file.jmx' in result.output


@mock.patch('wbplay_perftool.instance_controls.transfer_file')
@mock.patch('wbplay_perftool.instance_controls.configure_jmeter_user_properties')
@mock.patch('wbplay_perftool.remote.ssh_command')
def test_run_jmeterProperties_jmeterPropertiesUpdated_testRun(mock_ssh_command,
                                                              mock_configure_jmeter_user_properties,
                                                              mock_transfer_file,
                                                              runner,
                                                              moto_jmeter_cluster_instances,
                                                              test_jmx):
    # transfer file will be tested elsewhere, so its call is ignored
    mock_transfer_file.return_value = None

    # Make the call
    result = runner.invoke(wbplay_perftool.cli,
                           ['pytest-cluster', 'run', test_jmx, 'property1=value1', 'property2=value2'])

    # configure should be called three times, once for each node in our fixture cluster
    # assert mock_configure_jmeter_user_properties.call_args_list == []
    assert mock_configure_jmeter_user_properties.call_count == 3

    # Now we check for the jmeter properties
    # We go through the call arguments list and assert on the 2nd parameter (the jmeter properties) of each call
    for call in mock_configure_jmeter_user_properties.call_args_list:
        assert 'property1=value1' in call[0][1]
        assert 'property2=value2' in call[0][1]

    # Check that run was called
    assert mock_ssh_command.called

    # Then we check that the call completes with the proper status message
    assert result.exception is None
    assert 'Running JMeter' in result.output
    assert 'Dashboard URL' in result.output


def test_run_jmeterProperties_invalidProperty_clickBadParameterException(moto_jmeter_cluster_instances, runner,
                                                                         test_jmx):
    result = runner.invoke(wbplay_perftool.cli, ['pytest-cluster', 'run', test_jmx, 'woof'])

    assert result.exception
    assert "This doesn't appear to be a valid property (property=value): woof" in result.output


@mock.patch('wbplay_perftool.instance_controls.transfer_file')
@mock.patch('wbplay_perftool.instance_controls.configure_jmeter_user_properties')
@mock.patch('wbplay_perftool.remote.ssh_command')
def test_run_testFile_noJmeterProperties_testRun(mock_ssh_command,
                                                 mock_configure_jmeter_user_properties,
                                                 mock_transfer_file,
                                                 runner,
                                                 moto_jmeter_cluster_instances,
                                                 test_jmx):
    # transfer file will be tested elsewhere, so its call is ignored
    mock_transfer_file.return_value = None

    # Make the call
    result = runner.invoke(wbplay_perftool.cli, ['pytest-cluster', 'run', test_jmx])

    # Configure shouldn't have been called since we did not provide properties
    assert mock_configure_jmeter_user_properties.called is False

    # Check that run was called
    assert mock_ssh_command.called

    # Then we check that the call completes with the proper status message
    assert result.exception is None
    assert 'Running JMeter' in result.output
    assert 'Dashboard URL' in result.output


def test_run_profile_profileDoesNotExist_clickBadParameterException(moto_jmeter_cluster_instances, runner, test_jmx):
    # Make the call, use profile
    result = runner.invoke(wbplay_perftool.cli, ['pytest-cluster', 'run', '--profile', 'myprofile', test_jmx])

    # We should get an error
    assert result.exception
    assert 'The specified profile "myprofile" does not appear to be set up in your BOTO_CONFIG' in result.output


def test_run_clusterDoesNotExist_clickUsageError(runner, test_jmx):
    result = runner.invoke(wbplay_perftool.cli, ['pppppytest-cluster', 'run', test_jmx])

    # We should get an error
    assert result.exception
    assert 'JMeter cluster pppppytest-cluster does not exist! Please create it first with the up command.' in result.output


@mock.patch('wbplay_perftool.instance_controls.transfer_file')
@mock.patch('wbplay_perftool.instance_controls.configure_jmeter_user_properties')
@mock.patch('wbplay_perftool.remote.ssh_command')
def test_run_logfile_logfileUsed(mock_ssh_command,
                                 mock_configure_jmeter_user_properties,
                                 mock_transfer_file,
                                 runner,
                                 moto_jmeter_cluster_instances,
                                 test_jmx):

    # transfer file will be tested elsewhere, so its call is ignored
    mock_transfer_file.return_value = None

    # Make the call
    result = runner.invoke(wbplay_perftool.cli, ['pytest-cluster', 'run', '--logfile-name', 'test_result.csv', test_jmx])

    # Configure shouldn't have been called since we did not provide properties
    assert mock_configure_jmeter_user_properties.called is False

    # Check that run was called
    assert mock_ssh_command.called
    # It should have used our specified result file
    call_args = mock_ssh_command.call_args_list[0]
    assert '/home/jmeter/test_results/test_result.csv' in call_args[0][1]

    # Then we check that the call completes with the proper status message
    assert result.exception is None
    assert 'Running JMeter' in result.output
    assert 'Dashboard URL' in result.output

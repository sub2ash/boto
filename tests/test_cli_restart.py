import mock
from wbplay_perftool import wbplay_perftool


@mock.patch('wbplay_perftool.remote.ssh_command')
def test_restart_clusterExists_JMeterRestarted(mock_ssh_command, runner, moto_jmeter_cluster_instances):
    result = runner.invoke(wbplay_perftool.cli, ['pytest-cluster', 'restart'])

    assert result.exception is None
    # SSH should have been called twice (once for each slave node)
    assert mock_ssh_command.call_count == 2
    assert 'Restarting jmeter-server on the following instances' in result.output
    assert 'Finished restarting instances' in result.output


def test_restart_clusterDoesNotExist_ClickUsageError(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-clusterrrrargh-matey"', 'restart'])

    assert result.exception
    assert 'JMeter cluster "pytest-clusterrrrargh-matey" does not exist! Did you destroy it?' in result.output


def test_restart_profile_profileDoesNotExist_clickBadParameterException(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'restart', '--profile', 'myprofile'])

    assert result.exception
    assert 'The specified profile "myprofile" does not appear to be set up in your BOTO_CONFIG' in result.output


@mock.patch('wbplay_perftool.wbplay_perftool.get_aws_config')
def test_restart_noPrivateKey_ClickUsageError(mock_get_aws_config, moto_aws_connection, runner,
                                              config_no_key_file_path):
    # Use the modified config file
    mock_get_aws_config.return_value = config_no_key_file_path

    # Call the command
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'restart'])

    # There should be an error regarding the SSH key path
    assert result.exception
    assert 'Error: Your config is missing the SSH key path. Please run the privatekey command to set it' \
           in result.output

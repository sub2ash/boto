import json
from mock import mock
from wbplay_perftool import wbplay_perftool
from wbplay_perftool.configuration import AwsConfiguration


def test_privatekey_invalidFile_errorReturned(runner):
    # Run command with file that doesn't exist
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", "privatekey", r'c:\nopenopenope.pem'])

    # We should get an error response
    assert result.exception
    assert r"That doesn't appear to be a valid file: c:\nopenopenope.pem" in result.output

@mock.patch('wbplay_perftool.configuration.AwsConfiguration.set_private_key')
@mock.patch('wbplay_perftool.configuration.AwsConfiguration.save_config')
def test_privatekey_validFile_configSaved(mock_save_config, mock_set_private_key, runner,test_private_key):
    # Run command, set private key
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", "privatekey", test_private_key])

    # We should get a good response back from the CLI
    assert result.exception is None
    assert 'SSH Private key written to config file' in result.output

    # Check that we were going to update and save the config
    assert mock_save_config.called
    assert mock_set_private_key.called


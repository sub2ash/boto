import os
import shutil
import pytest
from click.testing import CliRunner
import boto
from wbplay_perftool.configuration import AwsConfiguration

from moto import mock_ec2


@pytest.fixture
def duplicate_config(request):
    # Because we're using a test runner, change the cwd to where the test is
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    # Create a test config if it isn't already there
    test_config = '_unittest.config'
    shutil.copyfile('..\wbplay_perftool\config.json', os.path.join('..\wbplay_perftool', test_config))

    def clean_up():
        # cleanup - remove the test file
        os.remove(os.path.join('..\wbplay_perftool', test_config))

    request.addfinalizer(clean_up)

    return test_config


@pytest.fixture
def test_jmx(request):
    # Because we're using a test runner, change the cwd to where the test is
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    # Create a test jmx file
    filename = os.path.join('..\wbplay_perftool', 'test_jmx')
    with open(filename, 'w') as new_file:
        new_file.write('')

    def clean_up():
        # cleanup - remove the test file
        os.remove(filename)

    request.addfinalizer(clean_up)

    return filename


@pytest.fixture
def test_private_key(request):
    # Because we're using a test runner, change the cwd to where the test is
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    # Create a test jmx file
    filename = os.path.join('..\wbplay_perftool', 'test_privatekey.pem')
    with open(filename, 'w') as new_file:
        new_file.write('')

    def clean_up():
        # cleanup - remove the test file
        os.remove(filename)

    request.addfinalizer(clean_up)

    return filename


@pytest.fixture
def runner(request):
    runner_to_use = CliRunner()

    return runner_to_use


@pytest.fixture
def multiple_test_jmx(request):
    # Because we're using a test runner, change the cwd to where the test is
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    path_to_use = '..\wbplay_perftool'

    # Create a list of a couple test jmx files
    filenames = []
    for index in range(0, 2):
        filename = 'test_jmx{}.jmx'.format(str(index))
        filenames.append(os.path.join(path_to_use, filename))
        with open(filenames[index], 'w') as new_file:
            new_file.write('')

    def clean_up():
        # cleanup - remove the test files
        for file_to_delete in filenames:
            os.remove(file_to_delete)

    request.addfinalizer(clean_up)

    return os.path.join(path_to_use, '*.jmx')


@pytest.fixture
def config_no_key_file_path(request):
    no_key_dictionary = {
        "definition": {
            "environment": {
                "device_mapping": {
                    "/dev/sda1": {
                        "delete_on_termination": False,
                        "size": 30
                    }
                },
                "ebs_optimized": False,
                "key_pair_name": "swarm-us-east",
                "region": "us-east-1",
                "security_group": "sg-2a7b1d4f",
                "subnet_id": "subnet-50738727"
            },
            "templates": {
                "master": {
                    "ami_id": "ami-4702bb2c",
                    "instance_type": "c3.4xlarge"
                },
                "slave": {
                    "ami_id": "ami-f5ff479e",
                    "instance_type": "m3.xlarge"
                }
            },
            "title": "malcolm",
            "elk_host": "elk.swarm-wbplay.com",
            "elk_port": 25826,
            "SSH_username": "jmeter",
            "SSH_password": "!Turbine1"
        }
    }

    return AwsConfiguration(no_key_dictionary)


@pytest.fixture
def moto_aws_connection(request):

    my_mock_ec2 = mock_ec2()
    my_mock_ec2.start()

    aws_connection = boto.ec2.connect_to_region('us-east-1')

    def clean_up():
        my_mock_ec2.stop()

    request.addfinalizer(clean_up)

    return aws_connection


@pytest.fixture
def moto_jmeter_cluster_instances(moto_aws_connection):
    # aws_connection = boto.ec2.connect_to_region('us-east-1')
    moto_aws_connection.run_instances('pytest-cluster-master')
    moto_aws_connection.run_instances('pytest-cluster-slave-1')
    moto_aws_connection.run_instances('pytest-cluster-slave-2')

    # Apply tags to those instances
    reservations = moto_aws_connection.get_all_instances()
    instances = [reservation.instances[0] for reservation in reservations]
    for instance in instances:
        instance.add_tag('JMeter Cluster Name', 'pytest-cluster')
        instance.add_tag('Name', instance.image_id)

    return instances

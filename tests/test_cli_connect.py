from wbplay_perftool import wbplay_perftool
import mock


def test_connect_noArgs_displayError(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ["connect"])

    print result.output
    assert 'Error: Missing command.' in result.output


def test_destroy_noArgs_helpDisplayed(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ["connect", "--help"])

    assert 'Usage: cli [OPTIONS] CLUSTER_NAME COMMAND' in result.output
    assert 'A command-line tool to automate deployment and' in result.output


def test_connect_clusterDoesNotExist_ClickUsageError(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", "connect"])

    assert "Error: Cannot find JMeter cluster pytest-cluster, did you already destroy it?" in result.output


@mock.patch('subprocess.Popen')
def test_connect_clusterExists_RDPSubprocessCalled(mock_subprocess, runner, moto_jmeter_cluster_instances):
    # Act
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", "connect"])

    # Assert
    assert mock_subprocess.called

    # Check the actual command. Can't test for IP because the IP always changes
    assert 'mstsc' in mock_subprocess.call_args[0][0]
    assert 'compute-1.amazonaws.com' in mock_subprocess.call_args[0][0]

    # Make sure console output is what we expect
    assert 'Connection Info' in result.output
    assert 'RDP Address' in result.output
    assert 'Username' in result.output
    assert 'Password' in result.output

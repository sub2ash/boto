import mock
from wbplay_perftool import wbplay_perftool


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
def test_transferFiles_badFilename_clickError(mock_get_instances_for_cluster, runner, moto_jmeter_cluster_instances):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Run the command
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'transfer_files', 'c:\my_house', '/your_house'])

    # We should get an error about the file filter
    assert result.exception
    assert 'There were no results for your file filter. Check your glob! Glob patterns follow Unix path expansion ' \
           'rules' in result.output


def test_transferFiles_badDestination_clickError(runner):
    # Need to check stderr from ssh_command used when transferring file
    # But then this would be an integration test - wouldn't it?
    # TODO: Not sure if this should be a unit test really
    pass


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
def test_transferFiles_nodeFilter_invalidFilter_clickError(mock_get_instances_for_cluster, runner,
                                                           moto_jmeter_cluster_instances):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Run the command
    result = runner.invoke(wbplay_perftool.cli,
                           ['"pytest-cluster"', 'transfer_files', '--node_filter', 'lol', 'c:\my_house', '/your_house'])

    assert result.exception
    assert "Invalid value for option --node_filter: 'lol'" in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.transfer_file')
def test_transferFiles_singleFile_noDestination_transferredCorrectly(mock_transfer_file, mock_get_instances_for_cluster,
                                                                     runner, moto_jmeter_cluster_instances, test_jmx):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Run the command
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'transfer_files', test_jmx])

    # Transfer file should have been called three times
    assert mock_transfer_file.call_count == 3
    # Check that each call has the correct file and destination
    for call in mock_transfer_file.call_args_list:
        assert test_jmx in call[0][1]
        assert '/home/jmeter/apache-jmeter-2.13/bin' in call[0][2]

    assert result.exception is None
    assert 'Transferring to' in result.output
    assert 'Finished transferring files' in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.transfer_file')
def test_transferFiles_listOfFiles_noDestination_transferredCorrectly(mock_transfer_file,
                                                                      mock_get_instances_for_cluster,
                                                                      runner, moto_jmeter_cluster_instances,
                                                                      multiple_test_jmx):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # multiple_text_jmx creates 2 .jmx files to be transferred
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'transfer_files', multiple_test_jmx])

    # Transfer files should have been called six times, 3 times per file transferred
    assert mock_transfer_file.call_count == 6

    # Check that each call has the correct file and destination
    for call in mock_transfer_file.call_args_list:
        assert multiple_test_jmx[:-5] + 'test_jmx' in call[0][1]
        assert '/home/jmeter/apache-jmeter-2.13/bin' in call[0][2]

    assert result.exception is None
    assert 'Transferring to' in result.output
    assert 'Finished transferring files' in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.transfer_file')
def test_transferFiles_singleFile_withDestination_transferredCorrectly(mock_transfer_file,
                                                                       mock_get_instances_for_cluster,
                                                                       runner, moto_jmeter_cluster_instances,
                                                                       test_jmx):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Run the command
    result = runner.invoke(wbplay_perftool.cli,
                           ['"pytest-cluster"', 'transfer_files', test_jmx, '~/new_location/'])

    # Transfer file should have been 3 times
    assert mock_transfer_file.call_count == 3
    # Check that each call has the correct file and destination
    for call in mock_transfer_file.call_args_list:
        assert '..\\wbplay_perftool\\test_jmx' in call[0][1]
        assert '~/new_location/' in call[0][2]

    assert result.exception is None
    assert 'Transferring to' in result.output
    assert 'Finished transferring files' in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.transfer_file')
def test_transferFiles_listOfFiles_withDestination_transferredCorrectly(mock_transfer_file,
                                                                        mock_get_instances_for_cluster,
                                                                        runner, moto_jmeter_cluster_instances,
                                                                        multiple_test_jmx):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Run the command, multiple_test_jmx has a glob that includes 2 jmx files
    result = runner.invoke(wbplay_perftool.cli,
                           ['"pytest-cluster"', 'transfer_files', multiple_test_jmx, '~/new_location/'])

    # Transfer file should have been 6 times, 3 times per file to be transfered
    assert mock_transfer_file.call_count == 6
    # Check that each call has the correct file and destination
    for call in mock_transfer_file.call_args_list:
        assert multiple_test_jmx[:-5] + 'test_jmx' in call[0][1]
        assert '~/new_location/' in call[0][2]

    assert result.exception is None
    assert 'Transferring to' in result.output
    assert 'Finished transferring files' in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.transfer_file')
def test_transferFiles_nodeFilter_m_transferredCorrectly(mock_transfer_file,
                                                         mock_get_instances_for_cluster,
                                                         runner, moto_jmeter_cluster_instances,
                                                         test_jmx):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Get the IP of the master node, so we can assert it later
    master_ip = [unicode(instance.ip_address) for instance in moto_jmeter_cluster_instances if
                 'master' in instance.image_id]

    # Run the command, multiple_test_jmx has a glob that includes 2 jmx files
    result = runner.invoke(wbplay_perftool.cli,
                           ['"pytest-cluster"', 'transfer_files', test_jmx, '--node_filter', 'm'])

    # Transfer file should have been once, because its just one file going to the master instance
    assert mock_transfer_file.call_count == 1

    for call in mock_transfer_file.call_args_list:
        # Check that we are affecting the right node
        assert master_ip[0] in call[0][0]

        # Check that each call has the correct file and destination
        assert '..\\wbplay_perftool\\test_jmx' in call[0][1]
        assert '/home/jmeter/apache-jmeter-2.13/bin' in call[0][2]

    assert result.exception is None
    assert 'Transferring to' in result.output
    assert 'Finished transferring files' in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.transfer_file')
def test_transferFiles_nodeFilter_sNumber_transferredCorrectly(mock_transfer_file,
                                                               mock_get_instances_for_cluster,
                                                               runner, moto_jmeter_cluster_instances,
                                                               test_jmx):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Get the IP of the slave node, so we can assert it later
    slave_ip = [unicode(instance.ip_address) for instance in moto_jmeter_cluster_instances if
                'slave-1' in instance.image_id]

    # Run the command
    result = runner.invoke(wbplay_perftool.cli,
                           ['"pytest-cluster"', 'transfer_files', test_jmx, '--node_filter', 's1'])

    # Transfer file should have been once, because we are only sending it to 1 slave
    assert mock_transfer_file.call_count == 1

    for call in mock_transfer_file.call_args_list:
        # Check that we are affecting the right node
        assert slave_ip[0] in call[0][0]

        # Check that each call has the correct file and destination
        assert '..\\wbplay_perftool\\test_jmx' in call[0][1]
        assert '/home/jmeter/apache-jmeter-2.13/bin' in call[0][2]

    assert result.exception is None
    assert 'Transferring to' in result.output
    assert 'Finished transferring files' in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.transfer_file')
def test_transferFiles_nodeFilter_sStar_transferredCorrectly(mock_transfer_file,
                                                             mock_get_instances_for_cluster,
                                                             runner, moto_jmeter_cluster_instances,
                                                             test_jmx):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Get the IP of the slave node, so we can assert it later
    slave_ip = [unicode(instance.ip_address) for instance in moto_jmeter_cluster_instances if
                'slave' in instance.image_id]

    # Run the command
    result = runner.invoke(wbplay_perftool.cli,
                           ['"pytest-cluster"', 'transfer_files', test_jmx, '--node_filter', 's*'])

    # Transfer file should have been twice, because the master node will not be included
    assert mock_transfer_file.call_count == 2

    for index, call in enumerate(mock_transfer_file.call_args_list):
        # Check that we are affecting the right node
        assert slave_ip[index] in call[0][0]
        # Check that each call has the correct file and destination
        assert '..\\wbplay_perftool\\test_jmx' in call[0][1]
        assert '/home/jmeter/apache-jmeter-2.13/bin' in call[0][2]

    assert result.exception is None
    assert 'Transferring to' in result.output
    assert 'Finished transferring files' in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.transfer_file')
def test_transferFiles_remap_sameNumberOfFilesAsNodes_transferredCorrectly(mock_transfer_file,
                                                                           mock_get_instances_for_cluster,
                                                                           runner, moto_jmeter_cluster_instances,
                                                                           multiple_test_jmx):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Run the command, multiple_test_jmx has a glob that includes 2 jmx files
    result = runner.invoke(wbplay_perftool.cli,
                           ['"pytest-cluster"', 'transfer_files', multiple_test_jmx, '--remap', 'user.csv'])

    # Transfer file should have been twice
    assert mock_transfer_file.call_count == 2

    for index, call in enumerate(mock_transfer_file.call_args_list):
        # Check that each call has the correct file and destination
        assert '..\\wbplay_perftool\\test_jmx{}.jmx'.format(index) in call[0][1]
        assert '/home/jmeter/apache-jmeter-2.13/bin\\user.csv' in call[0][2]

    assert result.exception is None
    assert 'Transferring to' in result.output
    assert 'Finished transferring files' in result.output


@mock.patch('wbplay_perftool.instance_controls.get_instances_for_cluster')
@mock.patch('wbplay_perftool.instance_controls.transfer_file')
def test_transferFiles_remap_differentNumberOfFilesToNodes_clickError(mock_transfer_file,
                                                                      mock_get_instances_for_cluster, runner,
                                                                      moto_jmeter_cluster_instances, test_jmx):
    # Make sure our moto instances are returned so we can pass the "is the cluster up?" check
    mock_get_instances_for_cluster.return_value = moto_jmeter_cluster_instances

    # Run the command
    result = runner.invoke(wbplay_perftool.cli,
                           ['"pytest-cluster"', 'transfer_files', '--remap', 'result.csv', test_jmx])

    # Check for error message
    assert result.exception
    assert 'You specified a remap but the amount of files is not the same size as the amount of nodes' in result.output


def test_up_profile_doesntExist_clickBadParameterException(runner):
    result = runner.invoke(wbplay_perftool.cli,
                           ['"pytest-cluster"', 'transfer_files', '--profile', 'crazy_bobs_discount_profile',
                            'c:\my_house', '/your_house'])

    assert result.exception
    assert 'The specified profile "crazy_bobs_discount_profile" does not appear to be set up in your BOTO_CONFIG' \
           in result.output


@mock.patch('wbplay_perftool.wbplay_perftool.get_aws_config')
def test_transferFiles_noPrivateKey_ClickUsageError(mock_get_aws_config, runner, config_no_key_file_path):
    # Use the modified config file
    mock_get_aws_config.return_value = config_no_key_file_path

    # Call the command
    result = runner.invoke(wbplay_perftool.cli, ['"pytest-cluster"', 'transfer_files', 'c:\my_house', '/your_house'])

    # There should be an error regarding the SSH key path
    assert result.exception
    assert 'Error: Your config is missing the SSH key path. Please run the privatekey command to set it' \
           in result.output

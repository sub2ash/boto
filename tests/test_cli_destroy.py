from moto import mock_ec2
from wbplay_perftool import wbplay_perftool


def test_destroy_noArguments_displayError(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ["destroy"])

    print result.output
    assert 'Error: Missing command.' in result.output


def test_destroy_noArguments_helpDisplayed(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ["destroy", "--help"])

    assert 'Usage: cli [OPTIONS] CLUSTER_NAME COMMAND' in result.output
    assert 'A command-line tool to automate deployment and' in result.output


def test_destroy_clusterName_helpDisplayed(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", "destroy", "--help"])

    # We should have Usage information
    assert 'Usage: cli destroy [OPTIONS]' in result.output
    assert 'The "destroy" command' in result.output


@mock_ec2
def test_destroy_nonExistantClusterName_errorMessage(moto_aws_connection, runner):
    result = runner.invoke(wbplay_perftool.cli, ["pytest-cluster", "destroy"])

    # We should expect an error message
    assert "Error: Could not find the instances to terminate. Do they exist?" in result.output


def test_destroy_existingClusterName_clusterDestroyed(runner, moto_jmeter_cluster_instances):
    # Act
    result = runner.invoke(wbplay_perftool.cli, ['pytest-cluster', 'destroy'])

    # Make sure the instances are terminated
    for instance in moto_jmeter_cluster_instances:
        # We have to call update() on the instances to get the current status
        instance.update()
        # And now they should show correctly that they are in a terminated state
        assert instance.state == 'terminated'

    # We should expect the following messages in the output if its successful
    assert "Terminating 'pytest-cluster' EC2 instances..." in result.output
    assert "Terminating Instances" in result.output
    assert "Done!" in result.output

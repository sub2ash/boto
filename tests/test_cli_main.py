import re
from wbplay_perftool import wbplay_perftool


def test_main_noArguments_helpDisplayed(runner):
    result = runner.invoke(wbplay_perftool.cli, [])

    # We should have Usage information
    assert 'Usage' in result.output


def test_main_clusterNameGiven_NoCommands_missingCommandMessage(runner):
    result = runner.invoke(wbplay_perftool.cli, ["jgarland-test"])

    print result.output
    assert 'Error: Missing command.' in result.output


def test_main_configFileGiven_noClusterNameGiven_missingClusterNameMessage(runner):
    result = runner.invoke(wbplay_perftool.cli, ['--config_file', 'config.json'])

    assert 'Error: Missing argument "cluster_name".' in result.output


def test_main_helpOption_helpDisplayed(runner):
    result = runner.invoke(wbplay_perftool.cli, ['--help'])

    # We should have Usage information
    assert 'Usage' in result.output


def test_main_configOption_configDoesNotExist_clickFileErrorRaised(moto_aws_connection, runner):
    # The CLI won't go further unless it has an actual "command" specified
    # So here we're going to just call connect even though it won't actually execute anything in connect
    # Because it will fail at the top level of the CLI structure
    bad_filename = 'myconfig.1111'

    result = runner.invoke(wbplay_perftool.cli, ['--config_file', bad_filename, '"jgarland-unittest"', 'connect'])

    # There should have been an exception
    assert result.exception
    # The exception should have the correct error message
    regex_result = re.match("Error: Could not open file (.*): The config file you specified does not exist!",
                            result.output)
    assert regex_result
    # The file path in the message should contain our filename we specified
    assert bad_filename in regex_result.group(1)


def test_main_configOption_configExists_configUsed(moto_aws_connection, duplicate_config, runner):
    # The CLI won't go further unless it has an actual "command" specified
    # So here we're going to just call connect to trigger it

    # Now run connect with a custom config value
    result = runner.invoke(wbplay_perftool.cli,
                           ['--config_file', duplicate_config, '"jgarland-unittest-should-not-exist"', 'connect'])

    # So here we're going to get an exception from connect.
    # But that is OK because that means click got past the top level 'main' group successfully
    assert result.exception
    assert 'Cannot find JMeter cluster "jgarland-unittest-should-not-exist", did you already destroy it?' in result.output

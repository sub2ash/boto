from setuptools import setup, find_packages

setup(
    name='wbplay-perftool',
    version='1.0.0',
    description='A command-line tool to automate deployment and management of a JMeter cluster in AWS.',
    url='https://github.turbine.com/WBNet/wbplay-perftool',
    author='Vorn Mom',
    author_email='vmom@turbine.com',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "mock==1.3.0",
        "Fabric==1.10.1",
        "boto==2.38.0",
        "click==5.1",
        "colorama==0.3.3",
        "gevent==1.0.2",
        "gipc==0.5.0",
        "greenlet==0.4.9",
        "paramiko==1.15.1",
        "requests==2.8.0",
        "pycrypto==2.6.1",
        "ecdsa==0.13",
        "pytest==2.8.2",
        "moto==0.4.18"
    ],
    entry_points={
        'console_scripts': [
            'wbplay-perftool=wbplay_perftool.wbplay_perftool:cli',
        ],
    },
    zip_safe=False)

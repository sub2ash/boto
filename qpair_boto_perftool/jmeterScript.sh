#!bin/sh
### BEGIN INIT INFO
# Provides:          jmeter fucntionality
# Short-Description: Apache JMeter start
# Description:       This file loadtest_runner.sh is to run jmeter fucntions
#                    jmeter_status(), jmeter_start(), view_log(), scripts_list()
#                    and jmeter_stop()
### END INIT INFO
#
# Usage:
#./loadtest_runner.sh [OPTIONS] {run_methodName} {run_jmx_path}
#
# Options:
# * -t   => Force the number of users threads
# * -l   => Force the number of loopCount
# * -r   => Force the ramp-up period
# * -d   => Force the duration
# * -h   => Force the usage
#
##############################################

#the current working directory is assigned to the scriptPath variable
scriptPath=$(pwd)
#function to run the jmx script and generate a jtl file
jmeter_start(){
printf "The jmeter_start() funciton is called\n"

#check for jmx and run if found
if [ -f "$1" ]; then
echo ""$1" found"
/home/ec2-user/apache-jmeter-3.0/bin/./jmeter -n -t "$1" -l "$2" $threadCount \
    $loopCount \
    $rampUp \
    $duration \

else
printf ""$1" not found, Please enter a valid .jmx fileName"
exit 1
fi
}
# Getting jmeter custom options.
n=1
while [ $n -le $# ]; do
#while [ "$1" != "" ]; do
case "$1" in
        -t)
 threadCount=" -JthreadCount=$2"
            shift 2
            ;;
        -l)
            loopCount=" -JloopCount=$2"
            shift 2
            ;;
        -r)
            rampUp=" -JrampUp=$2"
            shift 2
            ;;
        -d)
            duration=" -Jduration=$2"
            shift 2
            ;;
        *)
esac
n=$( expr $n + 1 )
done

#fucntion to help the user, displays the conditions in case statement
usage(){
    echo "Usage $0 {[-t threadCount]|[-l loopCount]|[-r rampUp]|[-d duration]|-h|start}"
}
####Main
case "$1" in

 start)
        shift
        jmeter_start "$1" "$2"
        ;;
-h | --help )
        usage
        exit
        ;;
  *) usage ;;
esac
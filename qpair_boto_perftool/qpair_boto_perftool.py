import os
import json
import glob
import subprocess
import click
import re
from configuration import AwsConfiguration
import boto3
import instance_controls
import output_handler
import remote
import botocore
from Exceptions import InvalidRegionError
import sys
import time
import datetime

class Context(object):
    def __init__(self):
        self.config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "default.config")


pass_context = click.make_pass_decorator(Context, ensure=True)


@click.group()
@click.option('--config_file', default="config.json", help="The environment configuration to use.")
@pass_context
def cli(ctx, config_file):
    ctx.config_file = os.path.join(os.path.dirname(__file__), config_file)

    if not os.path.isfile(ctx.config_file):
        raise click.FileError(ctx.config_file, 'The config file you specified does not exist!')


@cli.command(short_help="Spins up the instances. Default values are instance_type = t2.micro, security_group_id = sg-87b0a6ff, subnet_id = subnet-8f1da3f8, ebs_volume_size = 8gb, ebs_volume_type = io1 and ebs_iops = 240")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--image_id', default=None, help="The ImageId to use to create instances", type=str, prompt="ImageId to be used")
@click.option('--min_value', default=0, help="The min no of instances to be created", type=int, prompt="Min no.of instances")
@click.option('--max_value', default=0, help="The max no of instances to be created", type=int, prompt="Max no.of instances")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--config_file', default="config.json", help="The environment configuration to use.")
@pass_context
def create_instances(ctx, image_id, min_value, max_value, profile, region, config_file):
    try:
        ctx.config_file = os.path.join(os.path.dirname(__file__), config_file)

        if not os.path.isfile(ctx.config_file):
            raise click.FileError(ctx.config_file, 'The config file you specified does not exist!')

        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        # Start the instances
        instances = instance_controls.launch_instances(aws_config, image_id, min_value, max_value, session)
        output_handler.create_info()

        for instance in instances:
            output_handler.instance_info(instance.id)

    except AttributeError as e:
        print(e)


@cli.command(short_help="Spins up the instances. Default values are instance_type = m4.large, security_group_id = sg-87b0a6ff, subnet_id = subnet-8f1da3f8, ebs_volume_size = 8gb, ebs_volume_type = io1 and ebs_iops = 240")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--image_id', default=None, help="The ImageId to use to create instances", type=str, prompt="ImageId to be used")
@click.option('--min_value', default=0, help="The min no of instances to be created", type=int, prompt="Min no.of instances")
@click.option('--max_value', default=0, help="The max no of instances to be created", type=int, prompt="Max no.of instances")
@click.option('--placement_group', default='jmeter_test', help="The placement group for the instances", type=str, prompt="The placement group for the instances")
@click.option('--availability_zone', default='us-east-1c', help="The availability_zone for the instances", type=str, prompt="The availability zone for the instances")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--config_file', default="config.json", help="The environment configuration to use.")
@pass_context
def create_instances_with_placement_group(ctx, region, placement_group, availability_zone, image_id, min_value, max_value, profile, config_file):

    try:
        ctx.config_file = os.path.join(os.path.dirname(__file__), config_file)

        if not os.path.isfile(ctx.config_file):
            raise click.FileError(ctx.config_file, 'The config file you specified does not exist!')

        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        # Start the instances
        instances = instance_controls.launch_instances_with_placement_group(aws_config, placement_group, str(availability_zone), image_id, min_value, max_value, session)
        output_handler.create_info()
        for instance in instances:
            output_handler.instance_info(instance.id)
    except AttributeError as e:
        print(e)


@cli.command(short_help="starts the existing instance")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--instanceid', default=None, help="The ec2 instance to start.",type=str, prompt="instance id to start")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--config_file', default="config.json", help="The environment configuration to use.")
@pass_context
def start(ctx, region, instanceid, profile, config_file):
 
    try:
        ctx.config_file = os.path.join(os.path.dirname(__file__), config_file)

        if not os.path.isfile(ctx.config_file):
            raise click.FileError(ctx.config_file, 'The config file you specified does not exist!')

        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        output_handler.start_info()
   
        # Start an instance
        instance_controls.start_single_instance(aws_config,instanceid, session)
    except AttributeError as e:
        print(e)
        

@cli.command(short_help="Usage Details")
@click.help_option()
@pass_context
def usage(ctx):
    try:
        aws_config = get_aws_config(ctx)
 
        # usage of the commands
        instance_controls.usage_details()
    except AttributeError as e:
        print(e)
        



@cli.command(short_help="starts the provided instances list")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.argument('instanceids', default=None, nargs=-1,type=str)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def start_list(ctx, instanceids, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        output_handler.start_info()
   
        # Start a list of instances
        instances = instance_controls.start_instances(aws_config,instanceids, session)
    except AttributeError as e:
        print(e)
        


@cli.command(short_help="starts all the instances")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def start_all(ctx, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config, profile, region)

        output_handler.start_info()

        # Start all the instances
        instances = instance_controls.start_all_instances(aws_config, session)
    except AttributeError as e:
        print(e)



@cli.command(short_help="stops the provided instances list")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.argument('instanceids', default=None, nargs=-1,type=str)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def stop_list(ctx, instanceids, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        output_handler.stop_info()
       
        # Stop a list of instances
        instances = instance_controls.stop_instances(aws_config,instanceids, session)
    except AttributeError as e:
        print(e)



@cli.command(short_help="start an instance by name of the instances")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--name', default=None, help="The ec2 instance name to start.",type=str, prompt="Name of the instance")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def start_by_name(ctx, name, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config, profile, region)

        output_handler.start_info()

        # Start the instance by name
        instance_controls.start_single_instance_by_name(aws_config,name, session)
    except AttributeError as e:
        print(e)


@cli.command(short_help="start n instances in a placement group")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--groupname', default=None, help="The ec2 instance name to start.",type=str, prompt="The pacement group")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.argument('instanceids', default=None, nargs=-1,type=str)
@pass_context
def start_instances_by_placement_group(ctx, groupname, instanceids, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        output_handler.start_info()

        # Start the instances by placement group
        instance_controls.start_instances_by_PlacementGroup(aws_config, groupname, instanceids, session)
    except AttributeError as e:
        print(e)



@cli.command(short_help="stop n instances in a placement group")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--groupname', default=None, help="The ec2 instance name to stop.",type=str, prompt="The pacement group")
@click.argument('instanceids', default=None, nargs=-1,type=str)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def stop_instances_by_placement_group(ctx, groupname, instanceids, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        output_handler.stop_info()
       
        # Stop the instances by placement group
        instance_controls.stop_instances_by_PlacementGroup(aws_config, groupname, instanceids, session)
    except AttributeError as e:
        print(e)



@cli.command(short_help="start all instances in a placement group")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--groupname', default=None, help="The ec2 instance name to start.",type=str, prompt="The pacement group")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def start_all_instances_by_placement_group(ctx, groupname, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        output_handler.start_info()
       
        # Start all the instances by placement group
        instance_controls.start_all_instances_by_PlacementGroup(aws_config, groupname, session)
    except AttributeError as e:
        print(e)


@cli.command(short_help="stop all instances in a placement group")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--groupname', default=None, help="The ec2 instance name to start.",type=str, prompt="The pacement group")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def stop_all_instances_by_placement_group(ctx, groupname, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        output_handler.stop_info()

        # stop all the instances by placement group
        instance_controls.stop_all_instances_by_PlacementGroup(aws_config, groupname, session)
    except AttributeError as e:
        print(e)




@cli.command(short_help="start an instance by name of the instances")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--name', default=None, help="The ec2 instance name to start.",type=str, prompt="The name of the instance")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def stop_by_name(ctx, name, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        output_handler.stop_info()
       
        # stop an instance by name
        instance_controls.stop_single_instance_by_name(aws_config,name, session)
    except AttributeError as e:
        print(e)




@cli.command(short_help="stops the existing instance")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--instanceid', default=None, help="The ec2 instance to stop.",type=str, prompt="instance id to stop")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def stop(ctx, instanceid, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        # stop an instance
        output_handler.stop_info()

        instances = instance_controls.stop_single_instance(aws_config,instanceid, session)
    except AttributeError as e:
        print(e)

        

@cli.command(short_help="stops all the instances")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def stop_all(ctx, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        # stop all the instances
        output_handler.stop_info()

        instances = instance_controls.stop_all_instances(aws_config, session)
    except AttributeError as e:
        print(e)




@cli.command(short_help="describe the provided instance")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--instanceid', default=None, help="The ec2 instance to describe.",type=str, prompt="The instance id to be described")
@click.option('--format', default = None, help="The format to display the data", type=str, prompt="The format to describe the instance")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def describe(ctx, instanceid, format, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        # describe an instance
        output_handler.describeInfo()

        instance_controls.describe_instance(aws_config,instanceid,format, session)
    except AttributeError as e:
        print(e)



@cli.command(short_help="describe all the existing instances list")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--format', default = None, help="The format to display the data", type=str, prompt="The format to describe the instance")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def ec2_instances_details(ctx, format, profile, region):
    try:
        output_handler.instanceDetails()

        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        # instance details of all the instances

        instances = instance_controls.instances_details(aws_config,format, session)
    except AttributeError as e:
        print(e)





@cli.command(short_help="List of all the existing instance ids in an aws account")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def ec2_list(ctx, profile, region):
 
    try:
        if check_valid_region(region):

            output_handler.instanceIdsList()


            aws_config = get_aws_config(ctx)

            session = check_that_profile_exists(aws_config,profile, region)

            # ec2 list of instance ids
            instances = instance_controls.instances_list(aws_config, session)
    except AttributeError as e:
        print(e)


@cli.command(short_help="create images with default values of ebs_volume_size = 8gb, ebs_volume_type = io1 and ebs_iops = 240")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--instance_id', default = None, help="The instance id of the image", type=str, prompt='instance id to be used')
@click.option('--name', default = None, help="The name of the image", type=str, prompt='Name of the image')
@click.option('--description', default = 'An image', help="The description of the image", type=str)
@click.option('--ebs_volume_size', default=8, help="The ebs_volume_size to be used for an instance", type=str)
@click.option('--ebs_volume_type', default='io1', help="The ebs_volume_type to be used for an instance", type=str)
@click.option('--ebs_iops', default=240, help="The ebs_iops to be used for an instance", type=str)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def create_images(ctx, instance_id, name, description, ebs_volume_size, ebs_volume_type, ebs_iops, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        # create an image
        output_handler.createImage()

        instances = instance_controls.images(aws_config, instance_id, name, description, ebs_volume_size, ebs_volume_type, ebs_iops, session)
    except AttributeError as e:
        print(e)



@cli.command(short_help="The configuration of the provided asg")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--asg_name', default=None, help="The asg name to use",type=str, prompt='asg name to be used')
@click.option('--format', default=None, help="The format to display the data",type=str, prompt = 'format to decribe asg')
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def asg_config(ctx, asg_name, format, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        # asg configuration
        output_handler.asgConfig()

        instances = instance_controls.asg_configuration(aws_config, asg_name, format, session)
    except AttributeError as e:
        print(e)


    

@cli.command(short_help="The asg to be configured")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--asg_name', default=None, help="The asg name to use",type=str,prompt='The asg to be updated')
@click.option('--min_value', default=0, help="The min value to use for asg",prompt='min no of instances',type=int)
@click.option('--max_value', default=0, help="The max value to use for asg", prompt='max no of instances',type=int)
@click.option('--desired_value', default=0, help="The desired value to use for asg", prompt='desired no of instances',type=int)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def asg(ctx, asg_name, min_value, max_value, desired_value, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        # change asg config
        output_handler.changeASGConfig()

        instances = instance_controls.change_asg_config(aws_config, int(min_value), int(max_value), int(desired_value), asg_name, session)
    except AttributeError as e:
        print(e)




@cli.command(short_help="The configuration of the provided elb")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--elb_name', default=None, help="The elb name to use",type=str, prompt=True)
@click.option('--format', default=None, help="The format to display the data",type=str,prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def elb_config(ctx, elb_name, format, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
       
        # elb configuration
        output_handler.elbConfig()

        instances = instance_controls.elb_configuration(aws_config, elb_name, format, session)
    except AttributeError as e:
        print(e)



@cli.command(short_help="The list of instance ids in the elb provided")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--elb_name', default=None, help="The elb name to use",type=str, prompt='elb name to be used to display its instances list')
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def elb_list(ctx, elb_name, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
        # elb list of instance ids
        output_handler.elbList()

        instances = instance_controls.elb_instances_list(aws_config, elb_name, session)
    except AttributeError as e:
        print(e)




@cli.command(short_help="The details of instances in the elb provided")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--elb_name', default=None, help="The elb name to use",type=str,prompt='elb name to display details')
@click.option('--format', default=None, help="The format to display the data",type=str,prompt='The format to be used')
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def elb_list_details(ctx, elb_name, format, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        # elb details of the instances
        output_handler.elbListDetails()

        instances = instance_controls.elb_instances_details(aws_config, elb_name, format, session)
    except AttributeError as e:
        print(e)



@cli.command(short_help="The details of running instances list in an account")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str,prompt=True)
@click.option('--format', default=None, help="The format to display the data",type=str,prompt='The format to be used')
@click.option('--state', default=None, help="The state to be used like running|stopped|stopping|terminated|shutting-down",type=str,
                prompt='The state to be used like running|stopped|stopping|terminated|shutting-down')
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def instances_details_by_state(ctx, format, state, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        # details of the instances
        output_handler.instanceDetails()

        instances = instance_controls.instances(aws_config, format, state, session)
    except AttributeError as e:
        print(e)



@cli.command(short_help="To create a placement group")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--groupname', default=None, help="The name of the placement group",type=str,prompt='placement group name')
@click.option('--strategy', default='cluster', help="The placement Stratergy",type=str,prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def create_placement_group(ctx,groupname,strategy, profile, region):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        # Start the instances
        output_handler.create_Placement_Group()

        instances = instance_controls.create_PlacementGroup(aws_config, groupname, strategy, session)
    except AttributeError as e:
        print(e)




@cli.command(short_help="To describe an image")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--imageid', default=None, help="The image_id to describe",type=str,prompt="image id to be described")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--format', default=None, help="The format to display the data",type=str,prompt='The format to be used')
@pass_context
def describe_image(ctx, imageid, profile, region, format):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)

        # describe the image
        output_handler.describe_image()

        instances = instance_controls.image_details(aws_config, imageid, session, format)
    except AttributeError as e:
        print(e)


def check_valid_region(region):

    try:
        valid_regions = ['us-east-1','us-west-2','us-west-1','eu-west-1','eu-central-1','ap-southeast-1','ap-northeast-1','ap-northeast-2'
                                , 'ap-southeast-2','ap-south-1','sa-east-1']
        if not region in valid_regions:
            raise InvalidRegionError
    except InvalidRegionError:
        print("Invalid Region provided. Please provide a valid region name")
        sys.exit(-1)
        return False
    else:
        return True



def check_that_profile_exists(aws_config, profile, region):
    """
    Checks that the given profile exists in the caller's BOTO_CONFIG
    :param aws_config: AWSConfiguration object from get_aws_config
    :param profile: boto profile to check (string name of profile)
    :return: Nothing if profile exists. Raises click BadParameter if the profile does not exist
    """
    try:
        session = boto3.Session(profile_name=profile, region_name=region)
        return session
    except botocore.exceptions.ProfileNotFound as e:
        print(e)
    except botocore.exceptions.ClientError as e:
        click.echo(e)
        raise click.BadParameter(
            click.style('The specified profile "{}" does not appear to be set up in your BOTO_CONFIG'.format(profile),
                        fg='red'))


def get_aws_config(ctx):
    """
    Returns the configuration information from the given config file.
    :param ctx:
    :return:
    """
    # Load the default.
    data = open(ctx.config_file).read()
    configuration = json.loads(data)

    aws_configuration = AwsConfiguration(configuration)

    return aws_configuration



"""

@cli.command(short_help="Remote execute a test")
@click.argument('test_file', default=None, nargs=-1, type=str)
@click.option('--profile', default='default', help="The boto profile to use", type=str)
@click.option('--results_file', default='jmeter_results.csv', help="The name of the jmeter results file to use")
@click.option('--instances', default=0, help="The no of instances to be launched to use.")
@pass_context
def sequetial_run(ctx, test_file, instances, results_file, profile):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile)

        output_handler.run_tests_on_instances()

       
        if not len(test_file)==instances:
            click.echo("Please make sure the no of test files is equal to the instances count")
                
        if instances<=0:
            click.echo("Please provide instances count that is more than 0")
            # Step 1: Copy the jmx to the remote machine: /home/jmeter/apache-jmeter-2.13/bin/test.jmx
            # Make sure it really is a file
        for testFile in test_file:
            if not os.path.isfile(testFile):
                raise click.BadParameter(click.style('Not a valid file: {}'.format(testFile), fg='red'))
            file_name = os.path.basename(testFile)
            destination = "/home/ec2-user/apache-jmeter-3.0/bin/{}".format(file_name)
                
            # Step 2: Run the test.
            # Preparing the command
            jmeter_command = '/home/ec2-user/apache-jmeter-3.0/bin/./jmeter'
            results_file = '/home/ec2-user/apache-jmeter-3.0/bin/{}'.format(results_file)
            log_file = '/home/ec2-user/jmeter.log'
            local_destination = (os.path.split(os.path.realpath(file_name)))[0]
            remote_command = "{jmeter_command} -n -t {jmx_file} -l {results_file}" \
            .format(jmeter_command=jmeter_command, jmx_file=destination, results_file=results_file)
            click.echo(click.style("\nRunning JMeter", fg='green', bold=True))
            
            click.echo("Test plan is: "+str(testFile))    
            instance_controls.launch_instances_with_waiter_system_status_ok(aws_config, str(testFile), str(results_file), str(log_file),str(destination), str(local_destination), remote_command, session)

            output_handler.end_of_method()
    except AttributeError as e:
        click.echo("Invalid Profile. Please check the profile provided and try again.")
           
"""


@cli.command(short_help="Remote execute a test")
@click.argument('test_file', default=None,nargs=1, type=str)
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--results_file', default='jmeter_results.jtl', help="The name of the jmeter results file to use", prompt="result file name")
@click.option('--instances', default=0, help="The no of instances to be launched to use.")
@click.option('--instance_id', default=None, help="instance id to run the test", type=str)
@click.option('--terminate_instances', default='yes', help="yes to terminate instances and no to not terminate instances",prompt='--terminate_instances=yes|no', type = click.Choice(['yes','no']))
@click.option('--jmeter_properties_file', default=None, help="The jmeter properties file to update the jmeter properties on the instance",prompt="jmeter prope file")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def run_tests(ctx, test_file, instances, results_file, profile, jmeter_properties_file, region, instance_id, terminate_instances):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config, profile, region)

        output_handler.run_tests_on_instances()

        if test_file==None:
            click.echo("test file should be provided")
       

        if not os.path.isfile(str(test_file)):
                raise click.BadParameter(click.style('Not a valid file: {}'.format(str(test_file)), fg='red'))


        if not os.path.isfile(str(jmeter_properties_file)):
                raise click.BadParameter(click.style('Not a valid file: {}'.format(str(test_file)), fg='red'))

        file_name = os.path.basename(str(test_file))
        jmeter_prop_file = os.path.basename(str(jmeter_properties_file))
        destination = "/home/ec2-user/apache-jmeter-3.0/bin/{}".format(file_name)

        # Step 2: Run the test.
        # Preparing the command
        jmeter_command = '/home/ec2-user/apache-jmeter-3.0/bin/./jmeter'
        results_file = '/home/ec2-user/apache-jmeter-3.0/bin/{}'.format(str(results_file))
        log_file = '/home/ec2-user/jmeter.log'

        local_destination = (os.path.split(os.path.realpath(file_name)))[0]
        

        remote_command = "{jmeter_command} -n -t {jmx_file} -l {results_file}" \
        .format(jmeter_command=jmeter_command, jmx_file=destination, results_file=results_file)
        
        click.echo('\n')
        click.echo("launching "+str(instances)+" instances")
        if instance_id:
            ids_list = [str(instance_id)]
        else:
            ids_list = instance_controls.launch_instances_with_waiter_instances_status_ok(aws_config,instances, session)

        for instance_id in ids_list:
            click.echo('\n')
            click.echo("transferring files to instance "+str(instance_id))
            instance_controls.transfer_to_instances(aws_config, instance_id, str(test_file), str(destination), session)

        for instance_id in ids_list:
            click.echo('\n')
            click.echo("Updating jmeter user properties for instance "+str(instance_id))
            instance_controls.configure_jmeter_user_properties(aws_config, str(jmeter_properties_file), instance_id, session)

        for instance_id in ids_list:
            click.echo('\n')
            click.echo(click.style("\nRunning JMeter for "+str(instance_id), fg='green', bold=True))
            click.echo('\n')
            instance_controls.tests_operation(aws_config, instance_id, remote_command, session)

        for instance_id in ids_list:
            click.echo('\n')
            click.echo("transferring files from instance "+str(instance_id))
            instance_controls.transfer_from_instances(aws_config, instance_id, str(file_name), str(results_file), str(log_file), str(local_destination), session)
        
        if terminate_instances=='yes':
            for instance_id in ids_list:
                click.echo('\n')
                click.echo("terminating instance "+str(instance_id))
                instance_controls.terminate_instance_without_checks(aws_config, instance_id, session)
                             
        output_handler.end_of_method()
                
        if instances<=0:
            click.echo("Please provide instances count that is more than 0")
            # Step 1: Copy the jmx to the remote machine: /home/jmeter/apache-jmeter-2.13/bin/test.jmx
            # Make sure it really is a file

    except AttributeError as e:
        print(e)
     

@cli.command(short_help="Remote execute a test")
@click.argument('test_file', default=None,nargs=1, type=str)
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--results_file', default='test.jtl', help="The name of the jmeter results file to use", prompt="result file name")
@click.option('--instances', default=0, help="The no of instances to be launched to use.")
@click.option('--instance_id', default=None, help="instance id to run the test", type=str)
@click.option('--terminate_instances', default='yes', help="yes to terminate instances and no to not terminate instances",prompt='--terminate_instances=yes|no', type = click.Choice(['yes','no']))
@click.option('--jmeter_properties_file', default=None, help="The jmeter properties file to update the jmeter properties on the instance",prompt="jmeter prope file")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--threadcount', default=10, help="The threadCount for the test plan to be used", type=int, prompt=True)
@click.option('--rampup', default=3, help="The rampUp for the test plan to be used", type=int, prompt=True)
@click.option('--duration', default=10, help="The duration for the test plan to be used", type=int, prompt=True)
@pass_context
def run_tests_with_cloudwatchlogs(ctx, test_file, instances, results_file, profile, jmeter_properties_file, region, instance_id, terminate_instances, threadcount, rampup, duration):
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config, profile, region)

        output_handler.run_tests_on_instances()

        if test_file==None:
            click.echo("test file should be provided")
       

        if not os.path.isfile(str(test_file)):
                raise click.BadParameter(click.style('Not a valid file: {}'.format(str(test_file)), fg='red'))


        if not os.path.isfile(str(jmeter_properties_file)):
                raise click.BadParameter(click.style('Not a valid file: {}'.format(str(test_file)), fg='red'))

        file_name = os.path.basename(str(test_file))
        jmeter_prop_file = os.path.basename(str(jmeter_properties_file))
        destination = "/home/ec2-user/test/{}".format(file_name)

        # Step 2: Run the test.
        # Preparing the command
        jmeter_command = '/home/ec2-user/apache-jmeter-3.0/bin/./jmeter'
        results_file = '/home/ec2-user/test/{}'.format(str(results_file))
        log_file = '/home/ec2-user/jmeter.log'

        local_destination = (os.path.split(os.path.realpath(file_name)))[0]
        

        remote_command = "{jmeter_command} -n -t {jmx_file} -l {results_file} -JthreadCount={threadCount} -JrampUp={rampUp} -Jduration={duration}" \
        .format(jmeter_command=jmeter_command, jmx_file=destination, results_file=results_file, threadCount=threadcount, rampUp=rampup, duration=duration)
        
        click.echo('\n')
        click.echo("launching "+str(instances)+" instances")
        if instance_id:
            ids_list = [str(instance_id)]
        else:
            ids_list = instance_controls.launch_instances_with_waiter_instances_status_ok(aws_config,instances, session)

        for instance_id in ids_list:
            click.echo('\n')
            click.echo("transferring files to instance "+str(instance_id))
            instance_controls.transfer_to_instances(aws_config, instance_id, str(test_file), str(destination), session)

        for instance_id in ids_list:
            click.echo('\n')
            click.echo("Updating jmeter user properties for instance "+str(instance_id))
            instance_controls.configure_jmeter_user_properties(aws_config, str(jmeter_properties_file), instance_id, session)

        for instance_id in ids_list:
            click.echo('\n')
            click.echo(click.style("\nRunning JMeter for "+str(instance_id), fg='green', bold=True))
            click.echo('\n')
            print(datetime.datetime.now().time())
            instance_controls.tests_operation(aws_config, instance_id, remote_command, session)
            print(datetime.datetime.now().time())

        time.sleep(duration+60)

        for instance_id in ids_list:
            click.echo('\n')
            click.echo("transferring files from instance "+str(instance_id))
            instance_controls.transfer_from_instances(aws_config, instance_id, str(file_name), str(results_file), str(log_file), str(local_destination), session, duration)
        
        if terminate_instances=='yes':
            for instance_id in ids_list:
                click.echo('\n')
                click.echo("terminating instance "+str(instance_id))
                instance_controls.terminate_instance_without_checks(aws_config, instance_id, session)
                             
        output_handler.end_of_method()
                
        if instances<=0:
            click.echo("Please provide instances count that is more than 0")
            # Step 1: Copy the jmx to the remote machine: /home/jmeter/apache-jmeter-2.13/bin/test.jmx
            # Make sure it really is a file

    except AttributeError as e:
        print(e)
     

#--
@cli.command(short_help="Remote execute a gatling test")
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--terminate_instances', default='yes', help="yes to terminate instances and no to not terminate instances",prompt='--terminate_instances=yes|no', type = click.Choice(['yes','no']))
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--threadcount', default=10, help="The threadCount for the test plan to be used", type=int, prompt=True)
@click.option('--rampup', default=3, help="The rampUp for the test plan to be used", type=int, prompt=True)
@click.option('--config_file', default="gatling.json", help="The environment configuration to use.")
@pass_context
def run_tests_with_gatling(ctx, profile, region, terminate_instances, threadcount, rampup, config_file):
    try:
        ctx.config_file = os.path.join(os.path.dirname(__file__), config_file)

        if not os.path.isfile(ctx.config_file):
            raise click.FileError(ctx.config_file, 'The config file you specified does not exist!')
        
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config, profile, region)

        output_handler.run_tests_on_instances()

        instance_controls.gatling_test_on_instances(aws_config, session, threadcount, rampup, terminate_instances)
         
    except AttributeError as e:
        print(e)
#--


@cli.command(short_help="Remote execute a test")
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--terminate_instances', default='yes', help="yes to terminate instances and no to not terminate instances",prompt='--terminate_instances=yes|no', type = click.Choice(['yes','no']))
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--threadcount', default=10, help="The threadCount for the test plan to be used", type=int, prompt=True)
@click.option('--rampup', default=3, help="The rampUp for the test plan to be used", type=int, prompt=True)
@click.option('--config_file', default="jmeter.json", help="The environment configuration to use.")
@pass_context
def run_tests_with_jmeter(ctx, profile, region, terminate_instances, threadcount, rampup, config_file):
    try:
        ctx.config_file = os.path.join(os.path.dirname(__file__), config_file)

        if not os.path.isfile(ctx.config_file):
            raise click.FileError(ctx.config_file, 'The config file you specified does not exist!')

        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config, profile, region)

        output_handler.run_tests_on_instances()

        instance_controls.jmeter_test_on_instances(aws_config, session, threadcount, rampup, terminate_instances)

    except AttributeError as e:
        print(e)
     

   
@cli.command(short_help="terminates an existing instance")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--instanceid', default=None, help="The ec2 instance to start.",type=str, prompt="instance id to start")
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def terminate_instance(ctx, region, instanceid, profile):
 
    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
   
        # terminate the instance
        output_handler.terminate()

        instance_controls.terminate_single_instance(aws_config,instanceid, session)

    except AttributeError as e:
        print(e)
        

@cli.command(short_help="get the running instances names")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def get_running_instances_names(ctx, region, profile):

    try:
        aws_config = get_aws_config(ctx)

        session = check_that_profile_exists(aws_config,profile, region)
        # elb list of instance ids
        output_handler.running_instances_names()

        instances = instance_controls.get_running_state_instances_names(aws_config, session)
    except AttributeError as e:
        print(e)

@cli.command(short_help="To get AMI image list")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--owner', default='self', help="Owner of the images",type=str,prompt="owner of the AMI images")
@pass_context
def get_ami_images_list(ctx, owner, profile, region):
    try:
        aws_config = get_aws_config(ctx)
        session = check_that_profile_exists(aws_config,profile, region)
        # describe the image
        output_handler.ami_images_list()

        instances = instance_controls.ami_images_list(aws_config,  owner, session)

    except AttributeError as e:
        print(e)
        
@cli.command(short_help="The list of instance ids and state in the placement group provided")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--groupname', default=None, help="The name of the placement group",type=str,prompt='placement group name')
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@pass_context
def get_placement_group_ec2_list(ctx, groupname, profile, region):
    try:
        aws_config = get_aws_config(ctx)
        session = check_that_profile_exists(aws_config,profile, region)
        # placement group list of instance ids
        output_handler.placement_group_ec2_list()
        instances = instance_controls.placement_group_instances_list(aws_config, groupname, session)
    except AttributeError as e:
        print(e)

#@click.option('--metric_name', default='CPUUtilization', help="Metric Name", type=str, prompt=True)     metric_name, metric_name,

@cli.command(short_help="To get EC2 Metric Statistics")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--namespace', default='AWS/EC2', help="Name Space", type=str, prompt=True)
@click.option('--instanceid', default='i-49e62fd6', help="Instance ID",type=str,prompt="Instance ID")
@click.option('--statistics', default='Average', help="Statistics - Sum, Maximum, Minimum, Average",type=str,prompt="Statistics")
@click.option('--period', default=300, help="Period in seconds - Period must be a multiple of 60 and output should not exceed 1,440 datapoints", type=int, prompt="Period (seconds)")
@click.option('--timerange', default='1 hour', help="Time Range - 1 hour|3 hours|6 hours|12 hours|24 hours|1 week|2 weeks", type=str, prompt="Time Range- 1h|3h|6h|12h|24h|1w|2w")
@pass_context
def get_instance_metric_statistics(ctx, namespace,instanceid,period, profile, statistics, timerange, region):

    try:
        aws_config = get_aws_config(ctx)
        session = check_that_profile_exists(aws_config,profile, region)
        # describe the image
        output_handler.cw_metric_stats()
       
        instances = instance_controls.aws_instance_metric_statistics(aws_config, namespace, instanceid, statistics, period, timerange, session)

    except AttributeError as e:
        print(e)

@cli.command(short_help="To get EC2 CPU Metrics")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--namespace', default='AWS/EC2', help="Name Space", type=str, prompt=True)
@click.option('--instanceid', default='i-49e62fd6', help="Instance ID",type=str,prompt="Instance ID")
@click.option('--period', default=300, help="Period in seconds - Period must be a multiple of 60 and output should not exceed 1,440 datapoints", type=int, prompt="Period (seconds)")
@click.option('--timerange', default='1 hour', help="Time Range - 1 hour|3 hours|6 hours|12 hours|24 hours|1 week|2 weeks", type=str, prompt="Time Range- 1h|3h|6h|12h|24h|1w|2w")
@pass_context
def get_instance_cpu_metrics(ctx, namespace,instanceid,period, profile, timerange, region):

    try:
        aws_config = get_aws_config(ctx)
        session = check_that_profile_exists(aws_config,profile, region)
        # describe the image
        output_handler.cw_cpu_metrics()


        
        instances = instance_controls.aws_instance_cpu_metrics(aws_config,  namespace, instanceid, period, timerange, session)

    except AttributeError as e:
        print(e)

#
@cli.command(short_help="To get EC2 DiskReadBytes Metrics")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--instanceid', default='i-49e62fd6', help="Instance ID",type=str,prompt="Instance ID")
@click.option('--period', default=300, help="Period in seconds - Period must be a multiple of 60 and output should not exceed 1,440 datapoints", type=int, prompt=True)
@click.option('--timerange', default='1 hour', help="Time Range - 1 hour|3 hours|6 hours|12 hours|24 hours|1 week|2 weeks", type=str, prompt="Time Range- 1h|3h|6h|12h|24h|1w|2w")
@pass_context
def get_instance_diskreadbytes_metrics(ctx, instanceid, period, profile, timerange, region):
    try:
        aws_config = get_aws_config(ctx)
        session = check_that_profile_exists(aws_config,profile, region)
        # describe the image
        output_handler.cw_diskreadsbytes_metrics()
        
        instances = instance_controls.aws_instance_diskreadbytes_metrics(aws_config,  instanceid, period, timerange, session)

    except AttributeError as e:
        print(e)

#
@cli.command(short_help="To get EC2 DiskWriteBytes Metrics")
@click.help_option()
@click.option('--profile', default='default', help="The boto profile to use", type=str, prompt=True)
@click.option('--region', default='us-east-1', help="instances hosted region", type=str, prompt='region of hosted instances')
@click.option('--instanceid', default='i-49e62fd6', help="Instance ID",type=str,prompt="Instance ID")
@click.option('--period', default=300, help="Period in seconds - Period must be a multiple of 60 and output should not exceed 1,440 datapoints", type=int, prompt=True)
@click.option('--timerange', default='1 hour', help="Time Range - 1 hour|3 hours|6 hours|12 hours|24 hours|1 week|2 weeks", type=str, prompt="Time Range- 1h|3h|6h|12h|24h|1w|2w")
@pass_context
def get_instance_diskwritebytes_metrics(ctx, instanceid, period, profile, timerange, region):
    try:
        aws_config = get_aws_config(ctx)
        session = check_that_profile_exists(aws_config,profile, region)
        # describe the image
        output_handler.cw_diskwritesbytes_metrics()
        
        instances = instance_controls.aws_instance_diskwritebytes_metrics(aws_config,  instanceid, period, timerange, session)

    except AttributeError as e:
        print(e)

if __name__ == '__main__':
    cli(obj={})

#!bin/sh
# $1 for the path to gatling results directory
# $2 for the name of the scala file in lower case
# $3 for the path to simulation.log destination directory

tail -f "$1"/"$2"*/simulation.log > "$3"
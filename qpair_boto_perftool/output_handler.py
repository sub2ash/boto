import click
import json

def create_info():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Launch Info                                                 =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def instance_info(instance):
 
    click.echo(click.style(" Newly started instances: ", fg='white', bold=True) + 
        click.style(instance,fg='white', bold=True))
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def start_info():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Start Info                                                 =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def stop_info():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Stop Info                                                 =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def jsonFormat(response,indentValue):
    click.echo(click.style(json.dumps(response, indent=indentValue, sort_keys=True),fg='white', bold=True))
    

def textFormat(response):
    if isinstance(response,dict):
        for key1,val1 in response.items():
            if isinstance(val1,list)and val1:
                click.echo(click.style((str(key1)+':'+'\n'),fg='white', bold=True))
                textFormat(val1)
                click.echo('\n')
            elif isinstance(val1,dict)and val1:
                click.echo(click.style((str(key1)+':'+'\n'),fg='white', bold=True))
                textFormat(val1)
            else:
                click.echo(click.style((str(key1)+':'+str(val1)),fg='white', bold=True))
    elif isinstance(response,list):
        for elem in response:
            textFormat(elem)
    else:
        click.echo(click.style(("Please provide an existing one in your AWS account."),fg='white', bold=True))
 

def printMethod(dictionary,format):
    if str(format)=='text':
        click.echo(click.style(" Details in Text Format: ", fg='green', bold=True))
        textFormat(dictionary)
        click.echo(
        click.style("===================================================================", fg='green', bold=True))
    elif str(format)=='json':
        click.echo(click.style(" Details in Json Format: ", fg='green', bold=True))
        jsonFormat(dictionary,4)
    else:
        click.echo(click.style(("Please provide a valid format"),fg='white', bold=True))
        click.echo(
        click.style("===================================================================", fg='green', bold=True))


def describeInfo():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Describe Info                                                 =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def instanceIdsList():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Instance Ids                                                 =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def instanceDetails():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Instance Details                                                 =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def createImage():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Create Image                                                 =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def asgConfig():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= ASG Configuration Details                                                 =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def changeASGConfig():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Change ASG Configuration                                                 =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def elbConfig():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= ELB Configuration Details                                                =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def elbList():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= ELB instance ids                                                =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def elbListDetails():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= ELB instance details                                                =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def usageDetails():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Commands                                                =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def usageCommands():
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py usage --> usage details"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py start --profile {profile_name} --instanceid {instance_id} --region {region}--> start an instance"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py stop --profile {profile_name} --instanceid {instance_id} --region {region} --> stop the instance"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py ec2_list --profile {profile_name} --region {region} --> get ec2 instance ids list"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py describe --profile {profile_name} --region {region} --instanceid {instance_id} --format {format} --> describe details of an instance"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py ec2_instances_details --profile {profile_name} --region {region} --format {format} --> details of all the ec2 instances"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py start_list {instances seperated by space} --profile {profile_name} --region {region} --> start a list of instances"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py stop_list  {instances seperated by space} --profile {profile_name} --region {region} --> stop a list of instances"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py create_images --profile {profile_name} --instance_id {instance_id} --name {image_name} --region {region} --> create an image"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py create_instances --profile {profile_name} --image_id {image_id} --min_value {min_value} --max_value {max_value} --region {region} --> create instances"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py asg_config --profile {profile_name} --asg_name {asg_name} --region {region} --format {text|json} --> config details of asg"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py asg --profile {profile_name} --asg_name {asg_name} --region {region} --min_value {min_value} --max_value {max_value} --desired_value {desired_value} --> change asg config values"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py elb_list --profile {profile_name} --elb_name {elb_name} --region {region} --> list of instance ids in elb"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py elb_config --profile {profile_name} --elb_name {elb_name} --region {region} --format {format} --> config details of elb"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py instances_details_by_state --profile {profile_name} --state {running|stopped|terminated|shutting-down|stopping} --region {region} --format {format} --> instances details by state"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py create_instances_with_placement_group --profile {profile_name} --image_id {image_id} --min_value {min_value} --max_value {max_value} --placement_group {placement_group} --availability_zone {availability_zone} --region {region} --> create instances with placement_group"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py create_placement_group --profile {profile_name} --groupname {groupname} --strategy {strategy} --region {region} --> create placement_group"),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py describe_image --profile {profile_name} --imageid {image_id} --region {region} --format {format} --> describe an image "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py elb_list_details --profile {profile_name} --elb_name {elb_name} --region {region} --format {format} --> instances details in an elb "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py --help --> help on all operations "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py run_tests {jmeter test plan path} --profile {profile_name} --results_file {results_file.jtl|results_file.csv} --instances {no of instances to be launched} --jmeter_properties_file {jmeter properties file path} --region {region} --> run tests on instances "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py start_all --profile {profile_name} --region {region} --> start all instances "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py start_all_instances_by_placement_group --profile {profile_name} --groupname {groupname} --region {region} --> start all instances by placement group "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py start_by_name --profile {profile_name} --name {instance_name} --region {region} --> start instance by name "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py start_instances_by_placement_group {instances seperated by space} --profile {profile_name} --groupname {group_name} --region {region} --> start instance by placement group "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py stop_all --profile {profile_name} --region {region} --> stop all instances "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py stop_all_instances_by_placement_group --profile {profile_name} --groupname {group_name} --region {region} --> stop instances by placement group "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py stop_by_name --profile {profile_name} --name {instance_name} --region {region}--> stop intance by name "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py stop_instances_by_placement_group {instances seperated by space} --profile {profile_name} --groupname {group_name} --region {region} --> stop instances by placement group "),fg='white', bold=True))
    click.echo("\n")
    click.echo(click.style(("python qpair_boto_perftool.py terminate_instance --profile {profile_name} --instanceid  {instance_id} --region {region}  --> terminate an instance "),fg='white', bold=True))
    click.echo("\n")



def run_tests_on_instances():
     click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
     click.echo(
        click.style("= Run tests on instances                                                =", fg='green', bold=True))
     click.echo(
        click.style("===================================================================", fg='green', bold=True))



def terminate():

    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Terminating instance                                                =", fg='green', bold=True))
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def describe_image():

    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Describe image                                                =", fg='green', bold=True))
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def create_Placement_Group():

    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Create Placement Group                                                =", fg='green', bold=True))
    click.echo(
        click.style("===================================================================", fg='green', bold=True))


def running_instances_names():

    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Running  Placement Group                                                =", fg='green', bold=True))
    click.echo(
        click.style("===================================================================", fg='green', bold=True))















def connect_info(master_hostname, ctx, user, password):
    if master_hostname:
        click.echo(
            click.style("\n===================================================================", fg='green', bold=True))
        click.echo(
            click.style("= Connection Info                                                 =", fg='green', bold=True))
        click.echo(
            click.style("===================================================================", fg='green', bold=True))
        click.echo(click.style(" RDP Address: ", fg='green', bold=True) + click.style(master_hostname,
                                                                                      fg='white', bold=True))
        click.echo(click.style(" Username: ", fg='green', bold=True) + click.style(user, fg='white', bold=True))
        click.echo(
            click.style(" Password: ", fg='green', bold=True) + click.style(password, fg='white', bold=True))
        click.echo(
            click.style("===================================================================", fg='green', bold=True))
    else:
        click.echo(
            click.style("Cannot find JMeter cluster with name '" + ctx.cluster_name + "'.  Did you already destroy it?",
                        fg="red", bold=True))


def end_of_method():
    click.echo(
        click.style("========================================================================", fg='green', bold=True))


def placement_group_ec2_list():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= Placement Group ec2 instance ids                                  =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))
def ami_images_list():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= AMI Images owned by me                                            =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def cw_metric_stats():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= EC2 Instance Metric Statistics                                    =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def cw_diskreadsbytes_metrics():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= EC2  Disk Reads ( Bytes )  Metrics                                               =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def cw_diskwritesbytes_metrics():
    click.echo(
        click.style("\n===================================================================", fg='green', bold=True))
    click.echo(
        click.style("= EC2  Disk Writes ( Bytes )  Metrics                                               =", fg='green', bold=True))
  
    click.echo(
        click.style("===================================================================", fg='green', bold=True))

def transfer_file(instance, source, destination):
    click.echo(click.style(
        "Transferring to: {instance_ip} [{source} -> {destination}]".format(instance_ip=instance.ip_address,
                                                                            source=source,
                                                                            destination=destination), fg='green',
        bold=True))


def launch_list(nodes):
    # Give a list of nodes in the output
    instance_names = sorted([node['instance_name'] for node in nodes])
    for name in instance_names:
        click.echo(
            click.style("Launching EC2 instance: ", fg='green', bold=True) + click.style(name + "...", fg='green'))

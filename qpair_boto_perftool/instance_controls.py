import time
import boto3
import paramiko
import click
import remote
import output_handler
import os
import botocore
import json
import sys
import string
import threading
import datetime
import subprocess
import glob
import re
from configuration import AwsConfiguration
from Exceptions import InvalidRegionError
from sys import exit
from argparse import ArgumentParser
from datetime import timedelta
from datetime import datetime
from operator import itemgetter


elb_ids=[]
ids=[]
asg_config = dict()
elb_instances = dict()
elb_config = dict()
ec2_status = dict()
status_output = dict()
service = []
image_config = dict()
batch1_ids_list = []
batch2_ids_list = []


def launch_instances_with_waiter_instances_status_ok(aws_configuration,instances_count, session):
	environment = aws_configuration.get_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	waiter = client.get_waiter('instance_status_ok')

	#print(environment.AvailabilityZone)

	instances_id_list = get_instances_id_list(ec2, client, environment, waiter, instances_count)

	return instances_id_list


def get_instances_id_list(ec2, client, environment, waiter,instances_count):

	instances = ec2.create_instances(
		ImageId=environment.ImageId,
		MinCount=environment.MinCount,
		MaxCount=instances_count,
		KeyName=environment.KeyName,
		InstanceType=environment.InstanceType,
		SubnetId=environment.SubnetId,
		SecurityGroupIds=[environment.SecurityGroupId],
		Placement={
		'AvailabilityZone': (environment.AvailabilityZone),
		'GroupName': environment.PlacementGroup,
		'Tenancy': 'default'
		},
		BlockDeviceMappings=[{
		'DeviceName': '/dev/xvda',
		'Ebs':{
		'VolumeSize': environment.VolumeSize,
		'DeleteOnTermination': bool(environment.DeleteOnTermination),
		'VolumeType': environment.VolumeType,
		'Iops': environment.Iops
		}}]
		)

	instance_id_list = []
	for instance in instances:
		instance_id_list.append(instance.id)

	click.echo('\n')
	click.echo("waiting for instance reachability PASSED status...")
	waiter.wait(InstanceIds=instance_id_list,Filters=[{'Name':'instance-status.reachability', 'Values':['passed']}],IncludeAllInstances=True)

	for instanceId in instance_id_list:
		click.echo("DISPLAYING DETAILS OF "+str(instanceId)+" ===>")
		describe(client,instanceId,'text')

	return instance_id_list


def launch_jmeter_instances_with_waiter_instances_status_ok(aws_configuration,instances_count, session):
	environment = aws_configuration.get_jmeter_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	waiter = client.get_waiter('instance_status_ok')

	#print(environment.AvailabilityZone)

	instances_id_list = get_instances_id_list(ec2, client, environment, waiter, instances_count)

	return instances_id_list

	


def launch_gatling_instances_with_waiter_instances_status_ok(aws_configuration,instances_count, session):
	environment = aws_configuration.get_gatling_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	waiter = client.get_waiter('instance_status_ok')

	#print(environment.AvailabilityZone)

	instances_id_list = get_instances_id_list(ec2, client, environment, waiter, instances_count)

	return instances_id_list



def transfer_to_instances(aws_configuration, instance_id, test_file, destination, session):

	environment = aws_configuration.get_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')
	transfer_file_to_instance(PublicDnsName, test_file, destination, environment.PrivateKey, instance_id)
					

def transfer_to_jmeter_instances(aws_configuration, instance_id, test_file, destination, session):

	environment = aws_configuration.get_jmeter_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')
	transfer_file_to_instance(PublicDnsName, test_file, destination, environment.PrivateKey, instance_id)
					

def transfer_to_gatling_instances(aws_configuration, instance_id, test_file, destination, session):

	environment = aws_configuration.get_gatling_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')
	transfer_file_to_instance(PublicDnsName, test_file, destination, environment.PrivateKey, instance_id)
					

def transfer_from_instances(aws_configuration, instance_id, test_file, results_file, log_file, local_destination, session, test_count):

	environment = aws_configuration.get_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')
	transfer_file_from_instance(PublicDnsName, test_file, results_file, log_file, local_destination, environment.PrivateKey, instance_id, test_count)


def transfer_from_jmeter_instances(aws_configuration, instance_id, test_file, results_file, log_file, local_destination, session, test_count, merger_file):

	environment = aws_configuration.get_jmeter_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')
	transfer_file_from_jmeter_instance(PublicDnsName, test_file, results_file, log_file, local_destination, environment.PrivateKey, instance_id, test_count, merger_file)



def tests_operation(aws_configuration, instance_id, remote_command, session):

	environment = aws_configuration.get_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')

	remote.ssh_command(PublicDnsName, remote_command, environment.PrivateKey, 'ec2-user', show_output=True)
				

def jmeter_tests_operation(aws_configuration, instance_id, remote_command, session):

	environment = aws_configuration.get_jmeter_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')

	remote.ssh_command(PublicDnsName, remote_command, environment.PrivateKey, 'ec2-user', show_output=True)
				


def connection(aws_configuration,resourceConnectionType,clientConnectionType, session):
	
		if resourceConnectionType=='ec2_resource' and clientConnectionType=='ec2_client':
			service.append(session.resource('ec2'))
			service.append(session.client('ec2'))
			return service
		elif resourceConnectionType=='ec2_resource' and clientConnectionType==None:
			service.append(session.resource('ec2'))
			return service
		elif clientConnectionType=='asg_client' and resourceConnectionType==None:
			service.append(session.client('autoscaling'))
			return service
		elif clientConnectionType=='elb_client' and resourceConnectionType==None:
			service.append(session.client('elb'))
			return service
		else:
			click.echo("\n")
			click.echo("Please provide a valid group from ec2 or asg or elb")
			click.echo("\n")
	


def usage_details():

	output_handler.usageDetails()

	output_handler.usageCommands()

def launch_instances(aws_configuration, image_id, min_value, max_value, session):
	
	environment = aws_configuration.get_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	try:
		instances = ec2.create_instances(
			ImageId=image_id,
			MinCount=min_value,
			MaxCount=max_value,
			KeyName=environment.KeyName,
			InstanceType=environment.InstanceType,
			SubnetId=environment.SubnetId,
			SecurityGroupIds=[environment.SecurityGroupId],
			BlockDeviceMappings=[{
			'DeviceName': '/dev/xvda',
			'Ebs':{
			'VolumeSize': environment.VolumeSize,
			'DeleteOnTermination': bool(environment.DeleteOnTermination),
			'VolumeType': environment.VolumeType,
			'Iops': environment.Iops
			}}]
			)

		return instances
	except botocore.exceptions.ClientError as e:
		print(e)

def launch_instances_with_placement_group(aws_configuration, placement_group, availability_zone, image_id, min_value, max_value, session):
	
	environment = aws_configuration.get_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]


	try:

		instances = ec2.create_instances(
			ImageId=image_id,
			MinCount=min_value,
			MaxCount=max_value,
			KeyName=environment.KeyName,
			InstanceType=environment.InstanceType,
			SubnetId=environment.SubnetId,
			SecurityGroupIds=[environment.SecurityGroupId],
			Placement={
			'AvailabilityZone': availability_zone,
			'GroupName': placement_group,
			'Tenancy': 'default'
			},
			BlockDeviceMappings=[{
			'DeviceName': '/dev/xvda',
			'Ebs':{
			'VolumeSize': environment.VolumeSize,
			'DeleteOnTermination': bool(environment.DeleteOnTermination),
			'VolumeType': environment.VolumeType,
			'Iops': environment.Iops
			}}]
			)

		return instances
	except botocore.exceptions.ClientError as e:
		print(e)




def launch_instances_with_waiter_system_status_ok(aws_configuration, test_file, results_file, log_file, destination, local_destination, remote_command):

	environment = aws_configuration.get_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client')
	ec2 = service[0]
	client = service[1]

	waiter = client.get_waiter('instance_status_ok')


	instances = ec2.create_instances(
		ImageId=environment.ImageId,
		MinCount=environment.MinCount,
		MaxCount=environment.MaxCount,
		KeyName=environment.KeyName,
		InstanceType=environment.InstanceType,
		SubnetId=environment.SubnetId,
		SecurityGroupIds=[environment.SecurityGroupId],
		Placement={
		'AvailabilityZone': (environment.AvailabilityZone)[0],
		'GroupName': environment.PlacementGroup,
		'Tenancy': 'default'
		})

	for instance in instances:
		waiter.wait(InstanceIds=[instance.id],Filters=[{'Name':'instance-status.reachability', 'Values':['passed']}],IncludeAllInstances=True)
		response = client.describe_instances(InstanceIds=[instance.id])
		PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')
		describe(client,instance.id,'text')
		transfer_result_to_instance = transfer_file_to_instance(PublicDnsName, test_file, destination, environment.PrivateKey)
		remote.ssh_command(PublicDnsName, remote_command, environment.PrivateKey, 'ec2-user',show_output=True)
		transfer_result_from_instance = transfer_file_from_instance(PublicDnsName, results_file, log_file, local_destination, environment.PrivateKey)
		terminate_instances(client,instance.id)
					

def transfer_file_to_instance(PublicDnsName, test_file, destination, PrivateKey, instance_id):
	with paramiko.SSHClient() as ssh:
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(PublicDnsName, username='ec2-user', key_filename=PrivateKey)
		with paramiko.SFTPClient.from_transport(ssh.get_transport()) as sftp:
			sftp.put(test_file, destination)


def transfer_file_from_instance(PublicDnsName, test_file, results_file, log_file, destination, PrivateKey, instance_id, test_count):
	with paramiko.SSHClient() as ssh:
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(PublicDnsName, username='ec2-user', key_filename=PrivateKey)
		time_now = datetime.datetime.now().time()

		with paramiko.SFTPClient.from_transport(ssh.get_transport()) as sftp:
			local_destination_results_file = os.path.join(destination, instance_id+'_'+test_file+'_'+str(time_now)+'_'+os.path.basename(results_file))
			local_destination_log_file = os.path.join(destination, instance_id+'_'+test_file+'_'+str(time_now)+'_'+os.path.basename(log_file))
			#stdin,stdout,stderr = ssh.exec_command('ps -ef|grep loadtest_runner.sh|grep -v grep|awk '+'{print $2}')
			#if not stdout:
			#local_destination_results_file = os.path.join(destination,'test'+str(test_count)+'.jtl')
			
			print(datetime.datetime.now().time())
			sftp.get(results_file, local_destination_results_file)
			sftp.get(log_file, local_destination_log_file)


def transfer_file_from_jmeter_instance(PublicDnsName, test_file, results_file, log_file, destination, PrivateKey, instance_id, test_count, merger_file):
	with paramiko.SSHClient() as ssh:
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(PublicDnsName, username='ec2-user', key_filename=PrivateKey)
	#	time_now = datetime.datetime.now().time()
		time_now = datetime.datetime.now().time().strftime("%H-%M-%S")
		with paramiko.SFTPClient.from_transport(ssh.get_transport()) as sftp:
			local_destination_results_file = os.path.join(destination, instance_id+'_'+test_file+'_'+str(time_now)+'_'+os.path.basename(results_file))
			local_destination_log_file = os.path.join(destination, instance_id+'_'+test_file+'_'+str(time_now)+'_'+os.path.basename(log_file))
			#stdin,stdout,stderr = ssh.exec_command('ps -ef|grep loadtest_runner.sh|grep -v grep|awk '+'{print $2}')
			#if not stdout:
			#local_destination_results_file = os.path.join(destination,'test'+str(test_count)+'.jtl')
			
			print(datetime.datetime.now().time())
			sftp.get(results_file, local_destination_results_file)
			sftp.get(log_file, local_destination_log_file)

			click.echo("Adding jtl to the merger file")
			subprocess.Popen("echo 'inputJtl'"+str(test_count)+'='+local_destination_results_file+' >> '+merger_file,stdout=subprocess.PIPE, shell=True)





def exists(instanceId,ec2):
	ec2_instances(ec2)
	if instanceId in ids:
		return True
	else:
		return False


def start_all_instances(aws_configuration, session):
	
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	instances = ec2.instances.all()
	for instance in instances:
		start_stopped_instances(instance.id,ec2,client)


def stop_all_instances(aws_configuration, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	instances = ec2.instances.all()
	for instance in instances:
		stop_running_instances(instance.id,ec2,client)


def start_stopped_instances(instanceId,ec2,client):
		response = client.describe_instance_status(InstanceIds=[instanceId],IncludeAllInstances=True)
		Status = ((response.get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
		if Status=='running' or Status=='pending':
			click.echo("\n")
			click.echo("Instance already running. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='stopped':
			ec2.instances.filter(InstanceIds=[instanceId]).start()
			click.echo("\n")
			click.echo("Start operation performed. Please check the status of the instance. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='stopping':
			click.echo("\n")
			click.echo("Can't start the instance. It is in stopping stage now. Please wait for a moment. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='terminated' or Status=='shutting-down':
			click.echo("\n")
			click.echo("Instance is terminated. Instance ID is:"+str(instanceId))
			click.echo("\n")


def stop_running_instances(instanceId,ec2,client):
		response = client.describe_instance_status(InstanceIds=[instanceId],IncludeAllInstances=True)
		Status = ((response.get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
		if Status=='running' or Status=='pending':
			ec2.instances.filter(InstanceIds=[instanceId]).stop()
			click.echo("\n")
			click.echo("Stop operation performed. Please check the status of the instance. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='stopped' or Status=='stopping':
			click.echo("\n")
			click.echo("Can't Stop the instance. It is in stopping stage now. Please wait for a moment. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='terminated' or Status=='shutting-down':
			click.echo("\n")
			click.echo("Instance is terminated. Instance ID is:"+str(instanceId))
			click.echo("\n")


def start_instances(aws_configuration,list_instances, session):

	for instanceId in list_instances:
		start_single_instance(aws_configuration,instanceId, session)


def start_instances_by_PlacementGroup(aws_configuration, group_name, list_instances, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	for instanceId in list_instances:
		if exists(instanceId,ec2):
			response = client.describe_instances(InstanceIds=[instanceId])
			Name = ((((response.get('Reservations'))[0].get('Instances'))[0]).get('Placement')).get('GroupName')
			Status = ((((response.get('Reservations'))[0].get('Instances'))[0]).get('State')).get('Name')
			
			if Name==group_name:

				if Status=='running' or Status=='pending':
					click.echo("\n")
					click.echo("Instance already running. Instance ID is:"+str(instanceId))
					click.echo("\n")
				elif Status=='stopped':
					ec2.instances.filter(InstanceIds=[instanceId]).start()
					click.echo("\n")
					click.echo("Start operation performed. Please check the status of the instance. Instance ID is:"+str(instanceId))
					click.echo("\n")
				elif Status=='stopping':
					click.echo("\n")
					click.echo("Can't start the instance. It is in stopping stage now. Please wait for a moment. Instance ID is:"+str(instanceId))
					click.echo("\n")
				elif Status=='terminated' or Status=='shutting-down':
					click.echo("\n")
					click.echo("Instance is terminated. Instance ID is:"+str(instanceId))
					click.echo("\n")
		else:
			click.echo("\n")
			click.echo("The provided instance doesn't exist. Please provide an existing aws instance.")
			click.echo("\n")	



def stop_instances_by_PlacementGroup(aws_configuration, group_name, list_instances, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	for instanceId in list_instances:
		if exists(instanceId,ec2):
			response = client.describe_instances(InstanceIds=[instanceId])
			Name = ((((response.get('Reservations'))[0].get('Instances'))[0]).get('Placement')).get('GroupName')
			Status = ((((response.get('Reservations'))[0].get('Instances'))[0]).get('State')).get('Name')
			
			if Name==group_name:

				if Status=='running' or Status=='pending':
					click.echo("\n")
					ec2.instances.filter(InstanceIds=[instanceId]).start()
					click.echo("Stop operation performed. Please check the status of the instance. Instance ID is:"+str(instanceId))
					click.echo("\n")
				elif Status=='stopped' or Status=='stopping':
					click.echo("Can't stop the instance. It is in stopped or stopping stage now. Please wait for a moment. Instance ID is:"+str(instanceId))
					click.echo("\n")
				elif Status=='terminated' or Status=='shutting-down':
					click.echo("\n")
					click.echo("Instance is terminated. Instance ID is:"+str(instanceId))
					click.echo("\n")
		else:
			click.echo("\n")
			click.echo("The provided instance doesn't exist. Please provide an existing aws instance.")
			click.echo("\n")	



def start_all_instances_by_PlacementGroup(aws_configuration, group_name, session):

	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	id_list = get_all_placement_group_instances(ec2,group_name)
	for instanceId in id_list:
		Status = (((client.describe_instance_status(InstanceIds=[instanceId],IncludeAllInstances=True)).get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
		if Status=='running' or Status=='pending':
			click.echo("\n")
			click.echo("Instance already running. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='stopped':
			ec2.instances.filter(InstanceIds=[instanceId]).start()
			click.echo("\n")
			click.echo("Start operation performed. Please check the status of the instance. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='stopping':
			click.echo("\n")
			click.echo("Can't start the instance. It is in stopping stage now. Please wait for a moment. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='terminated' or Status=='shutting-down':
			click.echo("\n")
			click.echo("Instance is terminated. Instance ID is:"+str(instanceId))
			click.echo("\n")

def stop_all_instances_by_PlacementGroup(aws_configuration, group_name, session):

	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	id_list = get_all_placement_group_instances(ec2,group_name)
	for instanceId in id_list:
		Status = (((client.describe_instance_status(InstanceIds=[instanceId],IncludeAllInstances=True)).get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
		if Status=='running' or Status=='pending':
			ec2.instances.filter(InstanceIds=[instanceId]).stop()
			click.echo("\n")
			click.echo("Stop operation performed. Please check the status of the instance. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='stopped' or Status=='stopping':
			click.echo("\n")
			click.echo("Can't Stop the instance. It is in stopping stage now. Please wait for a moment. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='terminated' or Status=='shutting-down':
			click.echo("\n")
			click.echo("Instance is terminated. Instance ID is:"+str(instanceId))
			click.echo("\n")




def get_all_placement_group_instances(ec2,group_name):

	placement_group = ec2.PlacementGroup(group_name)
	instance_iterator = placement_group.instances.all()
	for instance in instance_iterator:
		ids.append(instance.id)
	return ids


def stop_instances(aws_configuration,list_instances, session):
	for instanceId in list_instances:
		stop_single_instance(aws_configuration,instanceId, session)


def start_single_instance_by_name(aws_configuration,name, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	instances = ec2.instances.filter(Filters=[{'Name': 'tag:Name', 'Values': [name]}])
	if not list(instances):
		print("No such instance available:"+str(name))
	elif instances:
		for instance in instances:
			Status = (((client.describe_instance_status(InstanceIds=[instance.id],IncludeAllInstances=True)).get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
			if Status=='running' or Status=='pending':
				click.echo("\n")
				click.echo("Instance already running. Instance ID is:"+str(instance.id))
				click.echo("\n")
			elif Status=='stopped':
				ec2.instances.filter(InstanceIds=[instance.id]).start()
				click.echo("\n")
				click.echo("Start operation performed. Please check the status of the instance. Instance ID is:"+str(instance.id))
				click.echo("\n")
			elif Status=='stopping':
				click.echo("\n")
				click.echo("Can't start the instance. It is in stopping stage now. Please wait for a moment. Instance ID is:"+str(instance.id))
				click.echo("\n")
			elif Status=='terminated' or Status=='shutting-down':
				click.echo("\n")
				click.echo("Instance is terminated. Instance ID is:"+str(instance.id))
				click.echo("\n")


def stop_single_instance_by_name(aws_configuration,name, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	instances = ec2.instances.filter(Filters=[{'Name': 'tag:Name', 'Values': [name]}])
	if not list(instances):
		print("No such instance available:"+str(name))
	elif instances:
		for instance in instances:
			Status = (((client.describe_instance_status(InstanceIds=[instance.id],IncludeAllInstances=True)).get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
			
			if Status=='running' or Status=='pending':
				ec2.instances.filter(InstanceIds=[instance.id]).stop()
				click.echo("\n")
				click.echo("Stop operation performed. Please check the status of the instance. Instance ID is:"+str(instance.id))
				click.echo("\n")
			elif Status=='stopped' or Status=='stopping':
				click.echo("\n")
				click.echo("Can't Stop the instance. It is in stopping stage now. Please wait for a moment. Instance ID is:"+str(instance.id))
				click.echo("\n")
			elif Status=='terminated' or Status=='shutting-down':
				click.echo("\n")
				click.echo("Instance is terminated. Instance ID is:"+str(instance.id))
				click.echo("\n")


def start_single_instance(aws_configuration,instanceId, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	if exists(instanceId,ec2):
		response = client.describe_instance_status(InstanceIds=[instanceId],IncludeAllInstances=True)
		Status = ((response.get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
		if Status=='running' or Status=='pending':
			click.echo("\n")
			click.echo("Instance already running. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='stopped':
			ec2.instances.filter(InstanceIds=[instanceId]).start()
			click.echo("\n")
			click.echo("Start operation performed. Please check the status of the instance. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='stopping':
			click.echo("\n")
			click.echo("Can't start the instance. It is in stopping stage now. Please wait for a moment. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='terminated' or Status=='shutting-down':
			click.echo("\n")
			click.echo("Instance is terminated. Instance ID is:"+str(instanceId))
			click.echo("\n")
	else:
		click.echo("\n")
		click.echo("The provided instance doesn't exist. Please provide an existing aws instance.")
		click.echo("\n")


def stop_single_instance(aws_configuration,instanceId, session):

	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	if exists(instanceId,ec2):
		response = client.describe_instance_status(InstanceIds=[instanceId],IncludeAllInstances=True)
		Status = ((response.get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
		if Status=='running' or Status=='pending':
			ec2.instances.filter(InstanceIds=[instanceId]).stop()
			click.echo("\n")
			click.echo("Stop operation performed. Please check the status of the instance. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='stopped' or Status=='stopping':
			click.echo("\n")
			click.echo("Can't Stop the instance. It is in stopping stage now. Please wait for a moment. Instance ID is:"+str(instanceId))
			click.echo("\n")
		elif Status=='terminated' or Status=='shutting-down':
			click.echo("\n")
			click.echo("Instance is terminated. Instance ID is:"+str(instanceId))
			click.echo("\n")
	else:
		click.echo("The provided instance doesn't exist. Please provide an existing aws instance.")


def terminate_single_instance(aws_configuration,instanceId, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	if exists(instanceId,ec2):
		response = client.describe_instance_status(InstanceIds=[instanceId],IncludeAllInstances=True)
		Status = ((response.get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
		if Status=='terminated' or Status=='shutting-down':
			click.echo("\n")
			click.echo("Instance is already terminated or shutting-down. Instance ID is:"+str(instanceId))
			click.echo("\n")
		else:
			click.echo("\n")
			response = client.terminate_instances(InstanceIds=[instanceId])
			click.echo("Instance is terminated. Instance ID is:"+str(instanceId))
			click.echo("\n")
	else:
		click.echo("The provided instance doesn't exist. Please provide an existing aws instance.")


def terminate_instance_without_checks(aws_configuration,instanceId, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	client.terminate_instances(InstanceIds=[instanceId])
	Status = (((client.describe_instance_status(InstanceIds=[instanceId],IncludeAllInstances=True)).get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
	if Status=='terminated' or Status=='shutting-down':
		click.echo('Instance '+str(instanceId)+' status is: '+Status)
		click.echo('Instance is terminated')
	else:
		click.echo('Instance '+str(instanceId)+' is not terminated...')


def describe_instance(aws_configuration,instanceId,format, session):

	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	if exists(instanceId,ec2):
		describe(client,instanceId,format)                              
	else:
		click.echo("\n")
		click.echo("The provided instance doesn't exist. Please provide an existing aws instance.")
		click.echo("\n")

def describe(client,instanceId,format):
	response = client.describe_instances(InstanceIds=[instanceId])
	#print(response)
	State = ((((response.get('Reservations'))[0].get('Instances'))[0]).get('State')).get('Name')
	describe_cases(client, response, State, instanceId)
	if (str(format))=="text":
		output_handler.printMethod(status_output,"text")
		click.echo("\n")       
		click.echo("End of Instance details...")
		click.echo("\n")
	elif (str(format))=="json":
		output_handler.printMethod(status_output,"json")
		click.echo("\n")       
		click.echo("End of Instance details...")
		click.echo("\n")
	else:
		click.echo("\n")       
		click.echo("Please provide a format of either json or text")
		click.echo("\n")


def instances_details(aws_configuration, format, session):

	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	ec2_instances(ec2)
	for instance in ids:
		click.echo(instance)
		describe(client,instance,format)


def instances(aws_configuration, format, state, session):
	
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	if state=='running':
		instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
		size = instancesSize(instances)
		if size==0:
			click.echo("No running instances")
		else:
			click.echo("Running instances are: "+str(size))
		for instance in instances:
			describe(client,instance.id,format)
	elif state=='stopping':
		instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['stopping']}])
		size = instancesSize(instances)
		if size==0:
			click.echo("No stopping instances")
		else:
			click.echo("stopping instances are: "+str(size))
		for instance in instances:
			describe(client,instance.id,format)
	elif state=='stopped':
		instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['stopped']}])
		size = instancesSize(instances)
		if size==0:
			click.echo("No stopped instances")
		else:
			click.echo("stopped instances are: "+str(size))
		for instance in instances:
			describe(client,instance.id,format)
	elif state=='terminated':
		instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['terminated']}])
		size = instancesSize(instances)
		if size==0:
			click.echo("No terminated instances")
		else:
			click.echo("terminated instances are: "+str(size))
		for instance in instances:
			describe(client,instance.id,format)
	elif state=='shutting-down':
		instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['shutting-down']}])
		size = instancesSize(instances)
		if size==0:
			click.echo("No shutting-down instances")
		else:
			click.echo("shutting-down instances are: "+str(size))
		for instance in instances:
			describe(client,instance.id,format)
	else:
		click.echo("Please provide a valid instance state")

 

def instances_list(aws_configuration, session):
	
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	ec2_instances(ec2)
	click.echo("\n")
	click.echo(ids)
	click.echo("\n")

def images(aws_configuration, instance_id, name, description, ebs_volume_size, ebs_volume_type, ebs_iops, session):
	try:
		connection(aws_configuration,'ec2_resource','ec2_client', session)
		ec2 = service[0]
		client = service[1]

		response = client.create_image(
			InstanceId=instance_id,
			Name=name,
			Description=description,
			BlockDeviceMappings=[{
			'DeviceName': '/dev/xvda',
			'Ebs':{
			'VolumeSize': ebs_volume_size,
			'DeleteOnTermination': True,
			'VolumeType': ebs_volume_type,
			'Iops': ebs_iops
			}}]
			)
		click.echo("\n")
		click.echo(response)
		click.echo("\n")

	except botocore.exceptions.ClientError as e:
			click.echo(type(e).__name__)
			#click.echo(e.__class__.__name__)
			click.echo("\n")
			click.echo(e)
			click.echo("\n")

def image_details(aws_configuration, imageId, session, format):
	try:
		connection(aws_configuration,'ec2_resource','ec2_client', session)
		ec2 = service[0]
		client = service[1]

		response = client.describe_images(ImageIds=[imageId])
		image_response(response)

		if format=='json':
			output_handler.printMethod(image_config,'json')
		elif format=='text':
			output_handler.printMethod(image_config,'text')

	except botocore.exceptions.ClientError as e:
			click.echo(type(e).__name__)
			#click.echo(e.__class__.__name__)
			click.echo("\n")
			click.echo(e)
			click.echo("\n")


def instancesSize(instances):
	size = []
	for instance in instances:
		size.append(instance.id)
	return len(size)

def ec2_instances(ec2):
	instances = ec2.instances.all()
	for instance in instances:
		ids.append(instance.id)
		#click.echo("InstanceId: "+instance.id)
	return ids

def asg_configuration(aws_configuration, asg_name, format, session):
	try:
		connection(aws_configuration,None,'asg_client', session)
		client_asg = service[0]

		response = client_asg.describe_auto_scaling_groups(
		AutoScalingGroupNames=[str(asg_name)])
		asg_response(response)
		if format=='json':
			output_handler.printMethod(asg_config,'json')
		elif format=='text':
			output_handler.printMethod(asg_config,'text')       
	except IndexError:
		click.echo("The auto scaling group you provided is not available. Please provide an existing auto scaling group")


def change_asg_config(aws_configuration, min_value, max_value, desired_value, asg_name, session):
	
	connection(aws_configuration,None,'asg_client', session)
	client_asg = service[0]

	if len(str(min_value))>=1 and len(str(max_value))>=1 and len(str(desired_value))>=1:
		if min_value!=None and max_value!=None and desired_value!=None:

			response = client_asg.update_auto_scaling_group(
			AutoScalingGroupName=asg_name,
			MinSize=min_value,
			MaxSize=max_value,
			DesiredCapacity=desired_value,
			)
			click.echo("\n")
			click.echo("Min_value= "+str(min_value)+", "+"Max_value= "+str(max_value)+", "+"Desired_value= "+str(desired_value))
			click.echo("Update operation performed on the auto scaling group. Please check the as_group configuration")
			click.echo("\n")
		else:
			click.echo("\n")
			click.echo("please provide proper values to update the auto scaling group")
			click.echo("\n")


def elb_configuration(aws_configuration, elb_name, format, session):
	
	connection(aws_configuration,None,'elb_client', session)
	client_elb = service[0]

	try:
		response = client_elb.describe_load_balancers(LoadBalancerNames=[str(elb_name)])
		elb_response(response)
		if format=='json':
			output_handler.printMethod(elb_config,'json')
		elif format=='text':
			output_handler.printMethod(elb_config,'text')
	except botocore.exceptions.ClientError as e:
		click.echo("\n")
		click.echo(e)
		click.echo("\n")


def elb_instances_list(aws_configuration, elb_name, session):

	connection(aws_configuration,None,'elb_client', session)
	client_elb = service[0]

	try:
		response = client_elb.describe_load_balancers(LoadBalancerNames=[str(elb_name)])     
		elb_instances_list = ((response.get('LoadBalancerDescriptions'))[0]).get('Instances')
		click.echo("Details in Json Format: ")
		for i in elb_instances_list:
			output_handler.jsonFormat(i, 2)      
	except botocore.exceptions.ClientError as e:
		click.echo("\n")
		click.echo(e)
		click.echo("\n")


def elb_instances_details(aws_configuration, elb_name, format, session):

	ec2_client = session.client('ec2')
	client_elb = session.client('elb')
	
	try:
		response = client_elb.describe_load_balancers(LoadBalancerNames=[str(elb_name)])     
		elb_instances_list = ((response.get('LoadBalancerDescriptions'))[0]).get('Instances')
		click.echo("Details in Json Format: ")
		for instanceId in elb_instances_list:
			describe(ec2_client,instanceId.get('InstanceId'),format)
	except botocore.exceptions.ClientError as e:
		click.echo("\n")
		click.echo(e)
		click.echo("\n")


def create_PlacementGroup(aws_configuration, GroupName, Strategy, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.create_placement_group(GroupName=GroupName,Strategy=Strategy)
	print(response)


def asg_response(response):

	asg_config['HealthCheckGracePeriod'] = ((response.get('AutoScalingGroups'))[0]).get('HealthCheckGracePeriod')
	asg_config['SuspendedProcesses'] = ((response.get('AutoScalingGroups'))[0]).get('SuspendedProcesses')
	asg_config['DesiredCapacity'] = ((response.get('AutoScalingGroups'))[0]).get('DesiredCapacity')
	asg_config['Tags'] = ((response.get('AutoScalingGroups'))[0]).get('Tags')
	asg_config['EnabledMetrics'] = ((response.get('AutoScalingGroups'))[0]).get('EnabledMetrics')
	asg_config['LoadBalancerNames'] = (', '.join(((response.get('AutoScalingGroups'))[0]).get('LoadBalancerNames')))
	asg_config['AutoScalingGroupName'] = ((response.get('AutoScalingGroups'))[0]).get('AutoScalingGroupName')
	asg_config['AutoScalingGroupARN'] = ((response.get('AutoScalingGroups'))[0]).get('AutoScalingGroupARN')
	asg_config['SuspendedProcesses'] = ((response.get('AutoScalingGroups'))[0]).get('SuspendedProcesses')
	asg_config['PlacementGroup'] = ((response.get('AutoScalingGroups'))[0]).get('PlacementGroup')
	asg_config['Status'] = ((response.get('AutoScalingGroups'))[0]).get('Status')
	asg_config['Instances'] = (', '.join(((response.get('AutoScalingGroups'))[0]).get('Instances')))
	asg_config['DefaultCooldown'] = ((response.get('AutoScalingGroups'))[0]).get('DefaultCooldown')
	asg_config['MinSize'] = ((response.get('AutoScalingGroups'))[0]).get('MinSize')
	asg_config['Instances'] = ((response.get('AutoScalingGroups'))[0]).get('Instances')
	asg_config['MaxSize'] = ((response.get('AutoScalingGroups'))[0]).get('MaxSize')
	asg_config['VPCZoneIdentifier'] = ((response.get('AutoScalingGroups'))[0]).get('VPCZoneIdentifier')
	asg_config['TerminationPolicies'] = (', '.join(((response.get('AutoScalingGroups'))[0]).get('TerminationPolicies')))
	asg_config['LaunchConfigurationName'] = ((response.get('AutoScalingGroups'))[0]).get('LaunchConfigurationName')
	asg_config['AvailabilityZones'] = (', '.join(((response.get('AutoScalingGroups'))[0]).get('AvailabilityZones')))
	asg_config['HealthCheckType'] = ((response.get('AutoScalingGroups'))[0]).get('HealthCheckType')
	asg_config['NewInstancesProtectedFromScaleIn'] = ((response.get('AutoScalingGroups'))[0]).get('NewInstancesProtectedFromScaleIn')

	return asg_config

def elb_response(response):

	elb_config['Subnets'] = (', '.join(((response.get('LoadBalancerDescriptions'))[0]).get('Subnets')))
	elb_config['Listener'] = ((response.get('LoadBalancerDescriptions'))[0]).get('ListenerDescriptions')[0].get('Listener')
	elb_config['Listener_PolicyNames'] = ((response.get('LoadBalancerDescriptions'))[0]).get('ListenerDescriptions')[0].get('PolicyNames')
	elb_config['AppCookieStickinessPolicies'] = (', '.join(((response.get('LoadBalancerDescriptions'))[0]).get('Policies').get('AppCookieStickinessPolicies')))
	elb_config['LBCookieStickinessPolicies'] = (', '.join(((response.get('LoadBalancerDescriptions'))[0]).get('Policies').get('LBCookieStickinessPolicies')))
	elb_config['OtherPolicies'] = (', '.join(((response.get('LoadBalancerDescriptions'))[0]).get('Policies').get('OtherPolicies')))
	elb_config['BackendServerDescriptions'] = (', '.join(((response.get('LoadBalancerDescriptions'))[0]).get('BackendServerDescriptions')))
	elb_config['AvailabilityZones'] = (', '.join(((response.get('LoadBalancerDescriptions'))[0]).get('AvailabilityZones')))
	elb_config['HealthCheck'] = ((response.get('LoadBalancerDescriptions'))[0]).get('HealthCheck')
	elb_config['VPCId'] = ((response.get('LoadBalancerDescriptions'))[0]).get('VPCId')
	elb_config['Instances'] = ((response.get('LoadBalancerDescriptions'))[0]).get('Instances')[0]
	elb_config['DNSName'] = ((response.get('LoadBalancerDescriptions'))[0]).get('DNSName')
	elb_config['SecurityGroups'] = (', '.join(((response.get('LoadBalancerDescriptions'))[0]).get('SecurityGroups')))
	elb_config['LoadBalancerName'] = ((response.get('LoadBalancerDescriptions'))[0]).get('LoadBalancerName')
	elb_config['CanonicalHostedZoneName'] = ((response.get('LoadBalancerDescriptions'))[0]).get('CanonicalHostedZoneName')
	elb_config['CanonicalHostedZoneNameID'] = ((response.get('LoadBalancerDescriptions'))[0]).get('CanonicalHostedZoneNameID')
	elb_config['Scheme'] = ((response.get('LoadBalancerDescriptions'))[0]).get('Scheme')
	elb_config['SourceSecurityGroup'] = ((response.get('LoadBalancerDescriptions'))[0]).get('SourceSecurityGroup')

	return elb_config

def image_response(response):

	image_config['VirtualizationType'] = (response.get('Images'))[0].get('VirtualizationType')
	image_config['Name'] = (response.get('Images'))[0].get('Name')
	image_config['Hypervisor'] = (response.get('Images'))[0].get('Hypervisor')
	image_config['EnaSupport'] = (response.get('Images'))[0].get('EnaSupport')
	image_config['SriovNetSupport'] = (response.get('Images'))[0].get('SriovNetSupport')
	image_config['ImageId'] = (response.get('Images'))[0].get('ImageId')
	image_config['State'] = (response.get('Images'))[0].get('State')
	image_config['Architecture'] = (response.get('Images'))[0].get('Architecture')
	image_config['ImageLocation'] = (response.get('Images'))[0].get('ImageLocation')
	image_config['RootDeviceType'] = (response.get('Images'))[0].get('RootDeviceType')
	image_config['Public'] = (response.get('Images'))[0].get('Public')
	image_config['ImageType'] = (response.get('Images'))[0].get('ImageType')
	image_config['Description'] = (response.get('Images'))[0].get('Description')
	image_config['EBS_DeviceName'] = ((response.get('Images'))[0].get('BlockDeviceMappings'))[0].get('DeviceName')
	image_config['EBS_DeleteOnTermination'] = ((response.get('Images'))[0].get('BlockDeviceMappings'))[0].get('Ebs').get('DeleteOnTermination')
	image_config['EBS_SnapshotId'] = ((response.get('Images'))[0].get('BlockDeviceMappings'))[0].get('Ebs').get('SnapshotId')
	image_config['EBS_VolumeSize'] = ((response.get('Images'))[0].get('BlockDeviceMappings'))[0].get('VolumeSize')
	image_config['EBS_VolumeType'] = ((response.get('Images'))[0].get('BlockDeviceMappings'))[0].get('VolumeType')
	image_config['EBS_Encrypted'] = ((response.get('Images'))[0].get('BlockDeviceMappings'))[0].get('Encrypted')
		
	return image_config


def describe_cases(client, response, State, instanceId):

	check_tags(client, instanceId)

	if State=='running' or State=='stopping':

		describe_response(response)

		status_output['PublicIpAddress'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('PublicIpAddress')

		return status_output

	elif State=='stopped' or State=='pending':

		describe_response(response)

		return status_output

	elif State=='terminated' or State=='shutting-down':
		click.echo("\n")
		click.echo("Instance is shut-down or terminated")
		click.echo("\n")



def check_tags(client, instanceId):

	tags_list = (client.describe_tags(Filters=[{'Name':'resource-id','Values':[instanceId]}])).get('Tags')

	if not len(tags_list)==0:
		
		status_output['Instance_Name'] = (tags_list[0]).get('Value')
		return status_output

	else:
		click.echo('\n')
		click.echo("No instance_name available for: "+str(instanceId))
		click.echo('\n')



def describe_response(response):

			status_output['OwnerId'] = (response.get('Reservations'))[0].get('OwnerId')
			status_output['Groups'] = (response.get('Reservations'))[0].get('Groups')
			status_output['PublicDnsName'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('PublicDnsName')
			status_output['RootDeviceType'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('RootDeviceType')
			status_output['Status'] = ((((response.get('Reservations'))[0].get('Instances'))[0]).get('State')).get('Name')
			status_output['EbsOptimized'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('EbsOptimized')
			status_output['PrivateIpAddress'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('PrivateIpAddress')
			status_output['VpcId'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('VpcId')
			status_output['StateTransitionReason'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('StateTransitionReason')
			status_output['InstanceId'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('InstanceId')
			status_output['ImageId'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('ImageId')
			status_output['PrivateDnsName'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('PrivateDnsName')
			status_output['KeyName'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('KeyName')
			status_output['Security_GroupName'] = ((((response.get('Reservations'))[0].get('Instances'))[0].get('SecurityGroups'))[0]).get('GroupName')
			status_output['Security_GroupId'] = ((((response.get('Reservations'))[0].get('Instances'))[0].get('SecurityGroups'))[0]).get('GroupId') 
			status_output['SubnetId'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('SubnetId')
			status_output['InstanceType'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('InstanceType')
			status_output['MacAddress'] = ((((response.get('Reservations'))[0].get('Instances'))[0].get('NetworkInterfaces'))[0]).get('MacAddress')
			status_output['Description'] = ((((response.get('Reservations'))[0].get('Instances'))[0].get('NetworkInterfaces'))[0]).get('Description')
			status_output['SourceDestCheck'] = ((((response.get('Reservations'))[0].get('Instances'))[0].get('NetworkInterfaces'))[0]).get('SourceDestCheck')
			status_output['AvailabilityZone'] = (((response.get('Reservations'))[0].get('Instances'))[0].get('Placement')).get('AvailabilityZone')
			status_output['Monitoring_State'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('Monitoring').get('State')
			status_output['ProductCodes'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('ProductCodes')
			status_output['EnaSupport'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('EnaSupport')
			status_output['NetworkInterfaceId'] = ((((response.get('Reservations'))[0].get('Instances'))[0].get('NetworkInterfaces'))[0]).get('NetworkInterfaceId')
			status_output['Attachment_DeleteOnTermination'] = ((((response.get('Reservations'))[0].get('Instances'))[0].get('NetworkInterfaces'))[0]).get('Attachment').get('DeleteOnTermination')
			status_output['AttachmentId'] = ((((response.get('Reservations'))[0].get('Instances'))[0].get('NetworkInterfaces'))[0]).get('Attachment').get('AttachmentId')
			status_output['Placement_Tenancy'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('Placement').get('Tenancy')
			status_output['Placement_GroupName'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('Placement').get('GroupName')
			status_output['Placement_AvailabilityZone'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('Placement').get('AvailabilityZone')
			status_output['Hypervisor'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('Hypervisor')
			status_output['EBS_DeviceName'] = (((response.get('Reservations'))[0].get('Instances'))[0].get('BlockDeviceMappings')[0]).get('DeviceName')
			status_output['EBS_Status'] = (((response.get('Reservations'))[0].get('Instances'))[0].get('BlockDeviceMappings')[0]).get('Ebs').get('Status')
			status_output['EBS_DeleteOnTermination'] = (((response.get('Reservations'))[0].get('Instances'))[0].get('BlockDeviceMappings')[0]).get('Ebs').get('DeleteOnTermination')
			status_output['EBS_VolumeId'] = (((response.get('Reservations'))[0].get('Instances'))[0].get('BlockDeviceMappings')[0]).get('Ebs').get('VolumeId')
			status_output['Architecture'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('Architecture')
			status_output['RootDeviceType'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('RootDeviceType')
			status_output['VirtualizationType'] = ((response.get('Reservations'))[0].get('Instances'))[0].get('VirtualizationType')

			return status_output


def jmeter_test_on_instances(aws_configuration, session, threadCount, rampUp, terminate_instances):
	
	environment = aws_configuration.get_jmeter_aws_environment()

	test_file = environment.jmx_file
	results_file = environment.results_file
	jmeter_properties_file = environment.jmeter_properties_file
	batch1_instances_count = environment.batch1_instances_count
	batch1_test_duration = environment.batch1_test_duration
	batch2_instances_count = environment.batch2_instances_count
	batch2_test_duration = environment.batch2_test_duration
	delay_time = environment.delay_time
	merger_file = environment.merger_file
	CMDRunner = environment.CMDRunner
	jmeter_path = environment.jmeter_path
	html_report_path = environment.html_report_path



	test_count = 0
	subprocess.Popen("echo > "+merger_file,stdout=subprocess.PIPE, shell=True)

	instances_count = batch1_instances_count + batch2_instances_count

	print(instances_count)

	isTestFile = os.path.isfile(test_file)
	isPropFile = os.path.isfile(jmeter_properties_file)

	if not os.path.isfile(test_file):
		raise click.BadParameter(click.style('Not a valid file: {}'.format(str(test_file)), fg='red'))

	if not os.path.isfile(jmeter_properties_file):
		raise click.BadParameter(click.style('Not a valid file: {}'.format(str(test_file)), fg='red'))

	file_name = os.path.basename(test_file)
	destination = "/home/ec2-user/test/{}".format(file_name)

	jmeter_command = '/home/ec2-user/apache-jmeter-3.0/bin/./jmeter'

	results_file = '/home/ec2-user/test/{}'.format(results_file)
	log_file = '/home/ec2-user/jmeter.log'

	local_destination = (os.path.split(os.path.realpath(file_name)))[0]

	subprocess.Popen('rm -rf -- '+html_report_path, shell=True)

	batch1_remote_command = "{jmeter_command} -n -t {jmx_file} -l {results_file} -JthreadCount={threadCount} -JrampUp={rampUp} -Jduration={duration} --addprop /home/ec2-user/apache-jmeter-3.0/bin/user.properties" \
	.format(jmeter_command=jmeter_command, jmx_file=destination, results_file=results_file,threadCount= threadCount, rampUp= rampUp, duration=batch1_test_duration)

	batch2_remote_command = "{jmeter_command} -n -t {jmx_file} -l {results_file} -JthreadCount={threadCount} -JrampUp={rampUp} -Jduration={duration} --addprop /home/ec2-user/apache-jmeter-3.0/bin/user.properties" \
	.format(jmeter_command=jmeter_command, jmx_file=destination, results_file=results_file,threadCount= threadCount, rampUp= rampUp, duration=batch2_test_duration)

	if max((delay_time+batch2_test_duration), batch1_test_duration)==batch1_test_duration:

		ids_list = launch_jmeter_instances_with_waiter_instances_status_ok(aws_configuration,instances_count, session)

		for id in ids_list:
			if not len(batch1_ids_list)>=batch1_instances_count:
				batch1_ids_list.append(id)
			else:
				batch2_ids_list.append(id)


		for instance_id in ids_list:
			click.echo('\n')
			click.echo("transferring files to instance "+str(instance_id))
			transfer_to_jmeter_instances(aws_configuration, instance_id, str(test_file), str(destination), session)

		click.echo('\n')
		click.echo("Performing tests on batch1 "+str(batch1_instances_count)+" instances")

		for instance_id in batch1_ids_list:
			click.echo('\n')
			click.echo(click.style("\nRunning JMeter for "+str(instance_id), fg='green', bold=True))
			click.echo('\n')
			click.echo("Updating jmeter user properties for instance "+str(instance_id))
			configure_jmeter_user_properties(aws_configuration, str(jmeter_properties_file), instance_id, session)
			click.echo('\n')
			print(datetime.datetime.now().time())
			jmeter_tests_operation(aws_configuration, instance_id, batch1_remote_command, session)
			print(datetime.datetime.now().time())

		time.sleep(delay_time)

		click.echo('\n')
		click.echo("Performing tests on batch2 "+str(batch2_instances_count)+" instances")

		for instance_id in batch2_ids_list:
			click.echo('\n')
			click.echo(click.style("\nRunning JMeter for "+str(instance_id), fg='green', bold=True))
			click.echo('\n')
			click.echo("Updating jmeter user properties for instance "+str(instance_id))
			configure_jmeter_user_properties(aws_configuration, str(jmeter_properties_file), instance_id, session)
			click.echo('\n')
			print(datetime.datetime.now().time())
			jmeter_tests_operation(aws_configuration, instance_id, batch2_remote_command, session)
			print(datetime.datetime.now().time())

		time.sleep(batch1_test_duration+120)

		for instance_id in ids_list:
			test_count+=1
			click.echo('\n')
			print(datetime.datetime.now().time())
			click.echo("transferring files from instance "+str(instance_id))
			transfer_from_jmeter_instances(aws_configuration, instance_id, str(file_name), str(results_file), str(log_file), str(local_destination), session, test_count, merger_file)

		click.echo("Merger Properties file")
		click.echo('\n')
	#	display = subprocess.Popen("cat "+merger_file, shell=True) #jmeter_log_transfer_windows
		display = subprocess.Popen("type "+merger_file, shell=True)
	#	time_now = datetime.datetime.now().time()
		time_now = datetime.datetime.now().time().strftime("%H-%M-%S")
	#	merge = subprocess.Popen('java -jar '+CMDRunner+' --tool Reporter --generate-csv Result_'+str(time_now)+'.csv --input-jtl '+merger_file+' --plugin-type MergeResults',stdout=subprocess.PIPE, shell=True)
		merge = subprocess.Popen(CMDRunner+' --tool Reporter --generate-csv Result_'+str(time_now)+'.csv --input-jtl '+merger_file+' --plugin-type MergeResults',stdout=subprocess.PIPE, shell=True)
		display.communicate()
		merge.communicate()

		if terminate_instances=='yes':
			for instance_id in batch1_ids_list:
				click.echo('\n')
				click.echo("terminating instance "+str(instance_id))
				terminate_instance_without_checks(aws_configuration, instance_id, session)

		subprocess.Popen('mkdir '+html_report_path, shell=True)

		click.echo("Generate HTML Report")
		click.echo('\n')
	#	report = subprocess.Popen("sh "+jmeter_path+" -g Result_"+str(time_now)+".csv -o "+html_report_path, shell=True)
		report = subprocess.Popen(jmeter_path+" -g Result_"+str(time_now)+".csv -o "+html_report_path, shell=True)
		report.communicate()

	else:
		click.echo("Delay Time + Duration of tests on second batch instances should be less than the duration of tests on first batch instances")

	output_handler.end_of_method()


def gatling_tests_operation(aws_configuration, instance_id, remote_command, session):

	environment = aws_configuration.get_gatling_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')

	with paramiko.SSHClient() as ssh:
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(PublicDnsName, username="ec2-user", key_filename=environment.PrivateKey)
		#ssh.exec_command('sudo rm -R /home/ec2-user/gatling-charts-highcharts-bundle-2.2.2/results/**')
		#ssh.exec_command('sudo rm ~/simulation.log')

	remote.ssh_command(PublicDnsName, remote_command, environment.PrivateKey, 'ec2-user', show_output=True)
				

#--def transfer_from_instances_gatling(aws_configuration, instance_id, test_file, log_file, local_destination, session, duration):
def transfer_from_gatling_instances(aws_configuration, instance_id, results_folder, scala_class_name, local_destination, session):

	environment = aws_configuration.get_gatling_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	print(" in the transfer_from_instances_g method")
	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')
	transfer_file_from_gatling_instance(PublicDnsName, scala_class_name, results_folder, local_destination, environment.PrivateKey, instance_id)
#--

#--

def transfer_file_from_gatling_instance(PublicDnsName, scala_class_name, results_folder, destination, PrivateKey, instance_id):
	with paramiko.SSHClient() as ssh:
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(PublicDnsName, username='ec2-user', key_filename=PrivateKey)
		time_now = datetime.datetime.now().time().strftime("%H-%M-%S")
		with paramiko.SFTPClient.from_transport(ssh.get_transport()) as sftp:
			print("In the sftp loop")
			copy_path = '/home/ec2-user/gatling-charts-highcharts-bundle-2.2.2/results/'+scala_class_name.lower()+'*/simulation.log'
			print copy_path
			ssh.exec_command('cp /home/ec2-user/gatling-charts-highcharts-bundle-2.2.2/results/'+scala_class_name.lower()+'*/simulation.log ~/.')
			source = '/home/ec2-user/simulation.log'
			local_destination_log_file = os.path.join(destination, 'simulations', instance_id+'_'+str(time_now)+'_'+scala_class_name+'.log')
			sftp.get(source, local_destination_log_file)


def stream_logs(aws_configuration, instance_id, results_folder, scala_class_name, session, total_sleep_time):

	environment = aws_configuration.get_gatling_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	time_count = 0

	print(" in the stream_logs method")
	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')
	#destination_path = 'home/ec2-user/gatling-charts-highcharts-bundle-2.2.2/results/simulation.log'
	with paramiko.SSHClient() as ssh:
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(PublicDnsName, username="ec2-user", key_filename=environment.PrivateKey)
		ssh.exec_command('touch '+results_folder+'simulation.log')
		while time_count<=total_sleep_time:
			ssh.exec_command('sh streamLogs.sh '+results_folder+' '+scala_class_name+' '+results_folder+'simulation.log > /dev/null 2>&1 &')
			time.sleep(1)
			time_count+=1
		

#--

def gatling_test_on_instances(aws_configuration, session, threadCount, rampUp, terminate_instances):
	environment = aws_configuration.get_gatling_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]


	scala_file = environment.scala_file
	batch1_instances_count = environment.batch1_instances_count
	batch1_test_duration = environment.batch1_test_duration
	batch2_instances_count = environment.batch2_instances_count
	batch2_test_duration = environment.batch2_test_duration
	delay_time = environment.delay_time
	gatling_exec_file_path = environment.gatling_exec_file_path

	instances_count = batch1_instances_count + batch2_instances_count

	isTestFile = os.path.isfile(scala_file)

	if not os.path.isfile(scala_file):
		raise click.BadParameter(click.style('Not a valid file: {}'.format(str(scala_file)), fg='red'))

	file_name = os.path.basename(str(scala_file))
	scala_class_name = os.path.splitext(str(file_name))[0]

	destination = "/home/ec2-user/gatling-charts-highcharts-bundle-2.2.2/user-files/simulations/{}".format(file_name)

	log_file = '/home/ec2-user/jmeter.log'

	gatling_command = '/home/ec2-user/gatling-charts-highcharts-bundle-2.2.2/bin/./gatling.sh'

	results_folder = '/home/ec2-user/gatling-charts-highcharts-bundle-2.2.2/results/'

	local_destination = (os.path.split(os.path.realpath(file_name)))[0]

	gatling_results_path = '/home/ec2-user/gatling-charts-highcharts-bundle-2.2.2/results/'

	gatling_log_directory = os.path.join(local_destination, 'simulations')

	total_sleep_time = batch1_test_duration+180
	
	for dirpath, dirnames, files in os.walk(gatling_log_directory):
		if files:
			subprocess.Popen('rm -rf -- '+gatling_log_directory+'/*', shell=True)
		else:
			click.echo("no files to delete in the simulations directory")

	batch1_remote_command = "JAVA_OPTS=\"-Dusers={threadCount} -Drampup={rampUp} -Dduration={duration}\" {gatling_command} -s {scala_file} -nr" \
	.format(gatling_command=gatling_command, scala_file=scala_class_name, threadCount=threadCount,rampUp=rampUp, duration=batch1_test_duration)

	batch2_remote_command = "JAVA_OPTS=\"-Dusers={threadCount} -Drampup={rampUp} -Dduration={duration}\" {gatling_command} -s {scala_file} -nr" \
	.format(gatling_command=gatling_command, scala_file=scala_class_name, threadCount=threadCount,rampUp=rampUp, duration=batch2_test_duration)

	if max((delay_time+batch2_test_duration), batch1_test_duration)==batch1_test_duration:
		ids_list = launch_gatling_instances_with_waiter_instances_status_ok(aws_configuration,instances_count, session)

		for id in ids_list:
			if not len(batch1_ids_list)>=batch1_instances_count:
				batch1_ids_list.append(id)
			else:
				batch2_ids_list.append(id)

		print(batch1_ids_list)

		print(batch2_ids_list)

		"""for instance_id in ids_list:
									click.echo('\n')
									click.echo("transferring files to instance "+str(instance_id))
									transfer_to_gatling_instances(aws_configuration, instance_id, str(scala_file), str(destination), session)
						"""
		click.echo('\n')
		click.echo("Performing tests on batch1 "+str(batch1_instances_count)+" instances")

		for instance_id in batch1_ids_list:
			click.echo('\n')
			click.echo("transferring files to instance "+str(instance_id))
			transfer_to_gatling_instances(aws_configuration, instance_id, str(scala_file), str(destination), session)
			click.echo('\n')
			click.echo(click.style("\nRunning Gatling for "+str(instance_id), fg='green', bold=True))
			click.echo('\n')
			print(datetime.datetime.now().time())
			gatling_tests_operation(aws_configuration, instance_id, batch1_remote_command, session)
			#stream_logs(aws_configuration, instance_id, results_folder, scala_class_name, session, total_sleep_time)
			print(datetime.datetime.now().time())

			
		time.sleep(delay_time)

		click.echo('\n')
		click.echo("Performing tests on batch2 "+str(batch2_instances_count)+" instances")

		for instance_id in batch2_ids_list:
			click.echo('\n')
			click.echo("transferring files to instance "+str(instance_id))
			transfer_to_gatling_instances(aws_configuration, instance_id, str(scala_file), str(destination), session)
			click.echo('\n')
			click.echo('\n')
			click.echo(click.style("\nRunning Gatling for "+str(instance_id), fg='green', bold=True))
			click.echo('\n')
			print(datetime.datetime.now().time())
			gatling_tests_operation(aws_configuration, instance_id, batch2_remote_command, session)
			#stream_logs(aws_configuration, instance_id, results_folder, scala_class_name, session, total_sleep_time)
			print(datetime.datetime.now().time())

		time.sleep(total_sleep_time)

		for instance_id in ids_list:
			click.echo('\n')
			print(datetime.datetime.now().time())
			click.echo("transferring files from instance "+str(instance_id))
			transfer_from_gatling_instances(aws_configuration, instance_id, str(results_folder), str(scala_class_name), str(local_destination), session)

		for instance_id in ids_list:
			click.echo('\n')
			click.echo("terminating instance "+str(instance_id))
			if terminate_instances=='yes':
				terminate_instance_without_checks(aws_configuration, instance_id, session)
			else:
				click.echo(instance_id+"is not terminated")

		click.echo("Merger Log files")
		click.echo('\n')
		merge = subprocess.call(gatling_exec_file_path+' -ro '+gatling_log_directory, shell=True)
	else:
		click.echo("test requirement is not met")

	output_handler.end_of_method()

def configure_jmeter_user_properties(aws_configuration, jmeter_properties_file, instance_id, session):

	environment = aws_configuration.get_jmeter_aws_environment()
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]

	response = client.describe_instances(InstanceIds=[instance_id])
	PublicDnsName = (((response.get('Reservations'))[0].get('Instances'))[0]).get('PublicDnsName')

	num_lines = sum(1 for line in open(str(jmeter_properties_file)))

	with paramiko.SSHClient() as ssh:
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(PublicDnsName, username="ec2-user", key_filename=environment.PrivateKey)

		properties_actual = "/home/ec2-user/apache-jmeter-3.0/bin/user.properties"

		ssh.exec_command('sudo rm '+properties_actual)

		ssh.exec_command('touch '+properties_actual)


		with open(str(jmeter_properties_file)) as lines:
			if len(str(num_lines)) > 0:
				for jmeter_property in lines:
					remote_command = 'echo "{jmeter_property}" | sudo tee -a {file}' \
					.format(jmeter_property=jmeter_property, file=properties_actual)
					click.echo(click.style("Adding Property: {}".format(jmeter_property), fg='green', bold=True))
					ssh.exec_command(remote_command)




def get_running_state_instances_names(aws_configuration, session):

	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	instances = ec2.instances.filter(Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
	for instance in instances:
		tags_list = (client.describe_tags(Filters=[{'Name':'resource-id','Values':[instance.id]}])).get('Tags')
		if not len(tags_list)==0:
			name =  (tags_list[0]).get('Value')
			print(name)
		else:
			click.echo('\n')
			click.echo("Instance with id --> "+str(instance.id)+" is running but no instance name is available for it.")
			click.echo('\n')


def ami_images_list(aws_configuration, owner, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	image_list = ec2.images.filter(Owners=[owner])
	for i in image_list:
		click.echo("\n")
		click.echo("AMI: "+str(i))
		click.echo("\n")
		
def placement_group_instances_list(aws_configuration, group_name, session):
	connection(aws_configuration,'ec2_resource','ec2_client', session)
	ec2 = service[0]
	client = service[1]
	id_list = get_all_placement_group_instances(ec2,group_name)
	for instanceId in id_list:
		Status = (((client.describe_instance_status(InstanceIds=[instanceId],IncludeAllInstances=True)).get('InstanceStatuses'))[0]).get('InstanceState').get('Name')
		if Status=='running' or Status=='pending':
			click.echo("\n")
			click.echo("Running Instance - Instance ID is: "+str(instanceId))
			click.echo("\n")
		elif Status=='stopped':
			click.echo("\n")
			click.echo("Stopped Instance - Instance ID is: "+str(instanceId))
			click.echo("\n")
#
def aws_instance_cpu_metrics(aws_configuration, name_space,instance_id,period, time_range,session):

	cloudwatch = session.client('cloudwatch')
	click.echo("Instance ID: "+str(instance_id)+"\n")

	metric_list = ['CPUUtilization','NetworkIn','NetworkOut','NetworkPacketsIn','NetworkPacketsOut','DiskWriteBytes','DiskReadBytes','DiskWriteOps','DiskReadOps','CPUCreditBalance','CPUCreditUsage','StatusCheckFailed','StatusCheckFailed_Instance','StatusCheckFailed_System']
	for metric_name in metric_list:
		#print (metric_name)
		now = datetime.utcnow()
		if time_range=='1 hour' or time_range=='1hour' or time_range=='1h' :
			past = now - timedelta(hours=1)
		elif time_range=='3 hour' or time_range=='1hour' or time_range=='3h' :
			past = now - timedelta(hours=3)
		elif time_range=='6 hours' or time_range=='6hours' or time_range=='6h':
		 	past = now - timedelta(hours=6)
		elif time_range=='12 hours' or time_range=='12hours' or time_range=='12h':
		  	past = now - timedelta(hours=12)
		elif time_range=='24 hours' or time_range=='24hours' or time_range=='24h' or time_range=='1d':
		  	past = now - timedelta(hours=24)
		elif time_range=='3 days' or time_range=='3days' or time_range=='3d':
		  	past = now - timedelta(days=3)
		elif time_range=='1 week' or time_range=='1week' or time_range=='1w':
		  	past = now - timedelta(weeks=1)
		elif time_range=='2 weeks' or time_range=='2weeks' or time_range=='2w':
		  	past = now - timedelta(weeks=2)
		else:
		  	click.echo("Invalid Time Range. Please Enter Valid Time Range - 1h, 3h, 6h, 24h, 1day(1d), 3days(3d), 1week(1w) or 2weeks(2w)")
		   	sys.exit(-1)


		results = cloudwatch.get_metric_statistics(
			Namespace=name_space,
			MetricName=metric_name,
			Dimensions=[{'Name': 'InstanceId', 'Value': instance_id}],
			StartTime=past,
			EndTime= now,	
			Period=period,
			Statistics=['Average','Sum','Minimum','Maximum'])
		datapoints = results['Datapoints']
		sorted_datapoints = sorted(datapoints, key=itemgetter('Timestamp'))
		click.echo(str(metric_name)+" Metrics:")
		for i in sorted_datapoints :
			click.echo("\n")
			click.echo (i)
			click.echo("\n")
#

def aws_instance_metric_statistics(aws_configuration, name_space,instance_id,stat,period, time_range,session):

	cloudwatch = session.client('cloudwatch')
	
	click.echo("Instance ID: "+str(instance_id))

	metric_list = ['CPUUtilization','NetworkIn','NetworkOut','NetworkPacketsIn','NetworkPacketsOut','DiskWriteBytes','DiskReadBytes','DiskWriteOps','DiskReadOps','CPUCreditBalance','CPUCreditUsage','StatusCheckFailed','StatusCheckFailed_Instance','StatusCheckFailed_System']
	stat_list = ['Sum', 'Maximum', 'Minimum', 'Average']
	for metric_name in metric_list:
		#print (metric_name)
		now = datetime.utcnow()
		if time_range=='1 hour' or time_range=='1hour' or time_range=='1h' :
			past = now - timedelta(hours=1)
		elif time_range=='3 hour' or time_range=='1hour' or time_range=='3h' :
			past = now - timedelta(hours=3)
		elif time_range=='6 hours' or time_range=='6hours' or time_range=='6h':
		 	past = now - timedelta(hours=6)
		elif time_range=='12 hours' or time_range=='12hours' or time_range=='12h':
		  	past = now - timedelta(hours=12)
		elif time_range=='24 hours' or time_range=='24hours' or time_range=='24h' or time_range=='1d':
		  	past = now - timedelta(hours=24)
		elif time_range=='3 days' or time_range=='3days' or time_range=='3d':
		  	past = now - timedelta(days=3)
		elif time_range=='1 week' or time_range=='1week' or time_range=='1w':
		  	past = now - timedelta(weeks=1)
		elif time_range=='2 weeks' or time_range=='2weeks' or time_range=='2w':
		  	past = now - timedelta(weeks=2)
		else:
		  	click.echo("Invalid Time Range. Please Enter Valid Time Range - 1h, 3h, 6h, 24h, 1day(1d), 3days(3d), 1week(1w) or 2weeks(2w)")
		   	sys.exit(-1)


		results = cloudwatch.get_metric_statistics(
			Namespace=name_space,
			MetricName=metric_name,
			Dimensions=[{'Name': 'InstanceId', 'Value': instance_id}],
			StartTime=past,
			EndTime= now,	
			Period=period,
			#Statistics=['Sum', 'Maximum', 'Minimum','Average'])
			Statistics=[stat])

		datapoints = results['Datapoints']
		sorted_datapoints = sorted(datapoints, key=itemgetter('Timestamp'))
		click.echo("\n")
		click.echo(str(metric_name)+" Metrics:")
		click.echo("\n")

		for i in sorted_datapoints :
			#click.echo("\n")
			click.echo (i)
			
#
def aws_instance_diskreadbytes_metrics(aws_configuration, instance_id, period, time_range,session):

	cloudwatch = session.client('cloudwatch')
	now = datetime.utcnow()
	if time_range=='1 hour' or time_range=='1hour' or time_range=='1h' :
		past = now - timedelta(hours=1)
	elif time_range=='3 hours' or time_range=='3hours' or time_range=='3h':
		past = now - timedelta(hours=3)
	elif time_range=='6 hours' or time_range=='6hours' or time_range=='6h':
		past = now - timedelta(hours=6)
	elif time_range=='12 hours' or time_range=='12hours' or time_range=='12h':
		past = now - timedelta(hours=12)
	elif time_range=='24 hours' or time_range=='24hours' or time_range=='24h' or time_range=='1d':
		past = now - timedelta(hours=24)
	elif time_range=='3 days' or time_range=='3days' or time_range=='3d':
		past = now - timedelta(days=3)
	elif time_range=='1 week' or time_range=='1week' or time_range=='1w':
		past = now - timedelta(weeks=1)
	elif time_range=='2 weeks' or time_range=='2weeks' or time_range=='2w':
		past = now - timedelta(weeks=2)

	results = cloudwatch.get_metric_statistics(
		Namespace='AWS/EC2',
		MetricName='DiskReadBytes',
		Dimensions=[{'Name': 'InstanceId', 'Value': instance_id}],
		StartTime=past,
		EndTime= now,	
		Period=period,
		Statistics=['Average','Sum','Minimum','Maximum'])
	#print(results)
	datapoints = results['Datapoints']
	sorted_datapoints = sorted(datapoints, key=itemgetter('Timestamp'))
	   
	click.echo("Instance ID: "+str(instance_id))

	for i in sorted_datapoints :
		click.echo("\n")
	#	click.echo("{0} load at {1}".format(load, timestamp))
		click.echo (i)
		click.echo("\n")
   
   #
def aws_instance_diskwritebytes_metrics(aws_configuration, instance_id, period, time_range,session):

	cloudwatch = session.client('cloudwatch')
	now = datetime.utcnow()
	if time_range=='1 hour' or time_range=='1hour' or time_range=='1h' :
		past = now - timedelta(hours=1)
	elif time_range=='3 hours' or time_range=='3hours' or time_range=='3h':
		past = now - timedelta(hours=3)
	elif time_range=='6 hours' or time_range=='6hours' or time_range=='6h':
		past = now - timedelta(hours=6)
	elif time_range=='12 hours' or time_range=='12hours' or time_range=='12h':
		past = now - timedelta(hours=12)
	elif time_range=='24 hours' or time_range=='24hours' or time_range=='24h' or time_range=='1d':
		past = now - timedelta(hours=24)
	elif time_range=='3 days' or time_range=='3days' or time_range=='3d':
		past = now - timedelta(days=3)
	elif time_range=='1 week' or time_range=='1week' or time_range=='1w':
		past = now - timedelta(weeks=1)
	elif time_range=='2 weeks' or time_range=='2weeks' or time_range=='2w':
		past = now - timedelta(weeks=2)

	results = cloudwatch.get_metric_statistics(
		Namespace='AWS/EC2',
		MetricName='DiskWriteBytes',
		Dimensions=[{'Name': 'InstanceId', 'Value': instance_id}],
		StartTime=past,
		EndTime= now,	
		Period=period,
		Statistics=['Average','Sum','Minimum','Maximum'])
	#print(results)
	datapoints = results['Datapoints']
	sorted_datapoints = sorted(datapoints, key=itemgetter('Timestamp'))
	   
	click.echo("Instance ID: "+str(instance_id))

	for i in sorted_datapoints :
		click.echo("\n")
	#	click.echo("{0} load at {1}".format(load, timestamp))
		click.echo (i)
		click.echo("\n")
 
import paramiko
import click
import time


def ssh_command(PublicDnsName, cmd, key_filename, username, show_output=True):
    """
    Run the given SSH command on the specified server
    :param node_ip: server to run command on (string)
    :param command: ssh command to run (string)
    :param key_filename: SSH key to use for authentication (string path/filename)
    :param username: ssh username to use (string)
    :param show_output: flag for if you want to show the stdout/stderr of the command or not (boolean)
    :return:
    """
    print(PublicDnsName, cmd, key_filename, username)
    with paramiko.SSHClient() as ssh:
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(PublicDnsName, username=username, key_filename=key_filename)
        # click.echo(click.style("SSH connected", fg='green', bold=True))
        channel = ssh.get_transport().open_session()
        #channel.settimeout(60)
        # click.echo(click.style("Running Command: " + command, fg='green', bold=True))
        command = 'sh ./jmeterScript.sh -t 10 -r 5 -d 15 start {jmx} {jtl} > /dev/null 2>&1 &'

        #channel.exec_command(command.format(jmx='/home/ec2-user/test/new_restapi_nginx.jmx', jtl='/home/ec2-user/test/test.jtl'))

        channel.exec_command(cmd+' > /dev/null 2>&1 &')

        # While we're not done with the command
        while not channel.exit_status_ready():
            print("In while loop")
            if show_output:
                print("In if loop of while loop")
                # General output
                if channel.recv_ready():
                    print("In if loop of recv_ready")
                    data = channel.recv(2048)
                    click.echo(click.style(data, fg='green', bold=True), nl=False)

                # Errors
                if channel.recv_stderr_ready():
                    print("In if loop of recv_stderr_ready")                    
                    data = channel.recv_stderr(2048)
                    click.echo(click.style(data, fg='green', bold=True), nl=False)
            time.sleep(3)

        

        # Flush remaining channel info if applicable
        if show_output:
            print("In flush if loop")
            if channel.recv_ready():
                print("In recv_ready flush if loop")
                click.echo(click.style(channel.recv(1048576), fg='green', bold=True), nl=False)
            if channel.recv_stderr_ready():
                print("In recv_stderr_ready flush if loop")
                click.echo(click.style(channel.recv_stderr(1048576), fg='green', bold=True), nl=False)





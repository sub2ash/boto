import json
import click

class Environment(object):
    def __init__(self, ImageId, MinCount, MaxCount, KeyName, InstanceType, SubnetId
                     , AvailabilityZone, PlacementGroup , PrivateKey, SecurityGroupId,  DeviceName, VolumeSize, DeleteOnTermination,
                       VolumeType, Iops):
        self.ImageId = ImageId
        self.MinCount = MinCount
        self.MaxCount = MaxCount
        self.KeyName = KeyName
        self.InstanceType = InstanceType
        self.SubnetId = SubnetId
        self.AvailabilityZone = AvailabilityZone
        self.PlacementGroup = PlacementGroup
        self.PrivateKey = PrivateKey
        self.SecurityGroupId = SecurityGroupId
        self.DeviceName = DeviceName
        self.VolumeSize = VolumeSize
        self.VolumeType = VolumeType
        self.DeleteOnTermination = DeleteOnTermination
        self.Iops = Iops


    def __str__(self):
        return "ImageId=%s MinCount=%s MaxCount=%s KeyName=%s InstanceType=%s SubnetId=%s AvailabilityZone=%s PlacementGroup=%s PrivateKey=%s SecurityGroupId=%s" % \
               (self.ImageId, str(self.MinCount), str(self.MaxCount), self.KeyName, self.InstanceType,
                self.SubnetId, self.AvailabilityZone, self.PlacementGroup ,self.PrivateKey, self.SecurityGroupId)

class Jmeter_Environment(object):
    def __init__(self, ImageId, MinCount, MaxCount, KeyName, InstanceType, SubnetId
                     , AvailabilityZone, PlacementGroup , PrivateKey, SecurityGroupId,  DeviceName, VolumeSize, DeleteOnTermination,
                       VolumeType, Iops, jmx_file, results_file, jmeter_properties_file, batch1_instances_count,
                       batch1_test_duration, batch2_instances_count, delay_time,
                       batch2_test_duration, merger_file, CMDRunner, jmeter_path, html_report_path):
        self.ImageId = ImageId
        self.MinCount = MinCount
        self.MaxCount = MaxCount
        self.KeyName = KeyName
        self.InstanceType = InstanceType
        self.SubnetId = SubnetId
        self.AvailabilityZone = AvailabilityZone
        self.PlacementGroup = PlacementGroup
        self.PrivateKey = PrivateKey
        self.SecurityGroupId = SecurityGroupId
        self.DeviceName = DeviceName
        self.VolumeSize = VolumeSize
        self.VolumeType = VolumeType
        self.DeleteOnTermination = DeleteOnTermination
        self.Iops = Iops
        self.jmx_file = jmx_file
        self.results_file = results_file
        self.jmeter_properties_file = jmeter_properties_file
        self.batch1_instances_count = batch1_instances_count
        self.batch1_test_duration = batch1_test_duration
        self.batch2_instances_count = batch2_instances_count
        self.batch2_test_duration = batch2_test_duration
        self.delay_time = delay_time
        self.merger_file = merger_file
        self.CMDRunner = CMDRunner
        self.jmeter_path = jmeter_path
        self.html_report_path = html_report_path

class Gatling_Environment(object):
    def __init__(self, ImageId, MinCount, MaxCount, KeyName, InstanceType, SubnetId
                     , AvailabilityZone, PlacementGroup , PrivateKey, SecurityGroupId,  DeviceName, VolumeSize, DeleteOnTermination,
                       VolumeType, Iops, batch1_instances_count,
                       batch1_test_duration, batch2_instances_count, delay_time,
                       batch2_test_duration, scala_file, gatling_exec_file_path):
        self.ImageId = ImageId
        self.MinCount = MinCount
        self.MaxCount = MaxCount
        self.KeyName = KeyName
        self.InstanceType = InstanceType
        self.SubnetId = SubnetId
        self.AvailabilityZone = AvailabilityZone
        self.PlacementGroup = PlacementGroup
        self.PrivateKey = PrivateKey
        self.SecurityGroupId = SecurityGroupId
        self.DeviceName = DeviceName
        self.VolumeSize = VolumeSize
        self.VolumeType = VolumeType
        self.DeleteOnTermination = DeleteOnTermination
        self.Iops = Iops
        self.batch1_instances_count = batch1_instances_count
        self.batch1_test_duration = batch1_test_duration
        self.batch2_instances_count = batch2_instances_count
        self.batch2_test_duration = batch2_test_duration
        self.delay_time = delay_time
        self.scala_file = scala_file
        self.gatling_exec_file_path = gatling_exec_file_path



class AwsConfiguration(object):
    def __init__(self, configuration):
        self.configuration = configuration

    def get_aws_environment(self):
        ImageId = self.configuration["definition"]["ImageId"]
        MinCount = self.configuration["definition"]["MinCount"]
        MaxCount = self.configuration["definition"]["MaxCount"]
        KeyName = self.configuration["definition"]["KeyName"]
        InstanceType = self.configuration["definition"]["InstanceType"]
        SubnetId = self.configuration["definition"]["SubnetId"]
        PrivateKey = self.configuration["definition"]["key_file_path"]
        AvailabilityZone = self.configuration["definition"]["AvailabilityZone"]
        PlacementGroup = self.configuration["definition"]["PlacementGroup"]
        DeviceName = self.configuration["definition"]["BlockDeviceMappings"]["DeviceName"]
        VolumeSize = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["VolumeSize"]
        DeleteOnTermination = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["DeleteOnTermination"]
        VolumeType = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["VolumeType"]
        Iops = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["Iops"]
        SecurityGroupId = self.configuration["definition"]["security_group_id"]
        

        return Environment(
            ImageId,
            MinCount,
            MaxCount,
            KeyName,
            InstanceType,
            SubnetId,
            str(AvailabilityZone),
            str(PlacementGroup),
            str(PrivateKey),
            str(SecurityGroupId),
            str(DeviceName),
            VolumeSize,
            DeleteOnTermination,
            str(VolumeType),
            Iops
        )

    def get_jmeter_aws_environment(self):
        ImageId = self.configuration["definition"]["ImageId"]
        MinCount = self.configuration["definition"]["MinCount"]
        MaxCount = self.configuration["definition"]["MaxCount"]
        KeyName = self.configuration["definition"]["KeyName"]
        InstanceType = self.configuration["definition"]["InstanceType"]
        SubnetId = self.configuration["definition"]["SubnetId"]
        PrivateKey = self.configuration["definition"]["key_file_path"]
        AvailabilityZone = self.configuration["definition"]["AvailabilityZone"]
        PlacementGroup = self.configuration["definition"]["PlacementGroup"]
        DeviceName = self.configuration["definition"]["BlockDeviceMappings"]["DeviceName"]
        VolumeSize = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["VolumeSize"]
        DeleteOnTermination = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["DeleteOnTermination"]
        VolumeType = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["VolumeType"]
        Iops = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["Iops"]
        SecurityGroupId = self.configuration["definition"]["security_group_id"]
        jmx_file = self.configuration["jmeterConfig"]["jmx_file"]
        results_file = self.configuration["jmeterConfig"]["results_file"]
        jmeter_properties_file = self.configuration["jmeterConfig"]["jmeter_properties_file"]
        batch1_instances_count = self.configuration["jmeterConfig"]["batch1_instances_count"]
        batch1_test_duration = self.configuration["jmeterConfig"]["batch1_test_duration"]
        batch2_instances_count = self.configuration["jmeterConfig"]["batch2_instances_count"]
        batch2_test_duration = self.configuration["jmeterConfig"]["batch2_test_duration"]
        delay_time = self.configuration["jmeterConfig"]["delay_time"]
        merger_file = self.configuration["jmeterConfig"]["merger_file"]
        CMDRunner = self.configuration["jmeterConfig"]["CMDRunner"]
        jmeter_path = self.configuration["jmeterConfig"]["jmeter_path"]
        html_report_path = self.configuration["jmeterConfig"]["html_report_path"]


        return Jmeter_Environment(
            ImageId,
            MinCount,
            MaxCount,
            KeyName,
            InstanceType,
            SubnetId,
            str(AvailabilityZone),
            str(PlacementGroup),
            str(PrivateKey),
            str(SecurityGroupId),
            str(DeviceName),
            VolumeSize,
            DeleteOnTermination,
            str(VolumeType),
            Iops,
            jmx_file,
            results_file,
            jmeter_properties_file,
            batch1_instances_count,
            batch1_test_duration,
            batch2_instances_count,
            delay_time,
            batch2_test_duration,
            merger_file,
            CMDRunner,
            jmeter_path,
            html_report_path
        )

    def get_gatling_aws_environment(self):
        ImageId = self.configuration["definition"]["ImageId"]
        MinCount = self.configuration["definition"]["MinCount"]
        MaxCount = self.configuration["definition"]["MaxCount"]
        KeyName = self.configuration["definition"]["KeyName"]
        InstanceType = self.configuration["definition"]["InstanceType"]
        SubnetId = self.configuration["definition"]["SubnetId"]
        PrivateKey = self.configuration["definition"]["key_file_path"]
        AvailabilityZone = self.configuration["definition"]["AvailabilityZone"]
        PlacementGroup = self.configuration["definition"]["PlacementGroup"]
        DeviceName = self.configuration["definition"]["BlockDeviceMappings"]["DeviceName"]
        VolumeSize = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["VolumeSize"]
        DeleteOnTermination = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["DeleteOnTermination"]
        VolumeType = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["VolumeType"]
        Iops = self.configuration["definition"]["BlockDeviceMappings"]["Ebs"]["Iops"]
        SecurityGroupId = self.configuration["definition"]["security_group_id"]
        batch1_instances_count = self.configuration["gatlingConfig"]["batch1_instances_count"]
        batch1_test_duration = self.configuration["gatlingConfig"]["batch1_test_duration"]
        batch2_instances_count = self.configuration["gatlingConfig"]["batch2_instances_count"]
        batch2_test_duration = self.configuration["gatlingConfig"]["batch2_test_duration"]
        delay_time = self.configuration["gatlingConfig"]["delay_time"]
        scala_file = self.configuration["gatlingConfig"]["scala_file"]
        gatling_exec_file_path = self.configuration["gatlingConfig"]["gatling_exec_file_path"]

        return Gatling_Environment(
            ImageId,
            MinCount,
            MaxCount,
            KeyName,
            InstanceType,
            SubnetId,
            str(AvailabilityZone),
            str(PlacementGroup),
            str(PrivateKey),
            str(SecurityGroupId),
            str(DeviceName),
            VolumeSize,
            DeleteOnTermination,
            str(VolumeType),
            Iops,
            batch1_instances_count,
            batch1_test_duration,
            batch2_instances_count,
            delay_time,
            batch2_test_duration,
            scala_file,
            gatling_exec_file_path
        )






"""def read_device_mapping(self, device_mapping):
                    mapping = BlockDeviceMapping()
                    for key in device_mapping.iterkeys():
                        mapping[key] = EBSBlockDeviceType(**(device_mapping[key]))
            
                    return mapping
            
                def get_all_nodes(self):
                    node_names = self.configuration["definition"]["environment"]["nodes"].keys()
                    return self._get_nodes(node_names)
            
                def get_node_metadata(self, node_name):
                    environment = self.get_aws_environment()
                    node = self.configuration["definition"]["environment"]["nodes"].get(node_name, None)
            
                    if isinstance(node, basestring):
                        # indicates template
                        templates = self.configuration["definition"].get("templates", {})
                        node = templates.get(node, None)
            
                    if not node:
                        return None
            
                    device_mapping = self.read_device_mapping(node["device_mapping"]) if node.get("device_mapping")\
                        else environment.device_mapping
            
                    descriptor_dictionary = {}
                    descriptor_dictionary["ami_id"] = node.get("ami_id", None)
                    descriptor_dictionary["instance_type"] = node.get("instance_type", None)
                    descriptor_dictionary["security_group"] = node.get("security_group", environment.security_group)
                    descriptor_dictionary["key_name"] = environment.key_pair_name
                    descriptor_dictionary["title"] = environment.title
                    descriptor_dictionary["instance_name"] = node_name
                    descriptor_dictionary["subnet_id"] = environment.subnet_id
                    descriptor_dictionary["ip_address"] = node.get("ip_address", None)
                    descriptor_dictionary["allocation_id"] = node.get("allocation_id", None)
                    descriptor_dictionary["placement"] = node.get("placement", None)
                    descriptor_dictionary["device_mapping"] = device_mapping
                    descriptor_dictionary["ebs_optimized"] = node.get("ebs_optimized", environment.ebs_optimized)
                    descriptor_dictionary["placement_group"] = node.get("placement_group", None)
            
                    return descriptor_dictionary
            
                def get_placement_groups(self):
                    return self.configuration.get("placement-groups", {}).copy()
            
                def _get_nodes(self, names):
                    nodes = []
                    for name in names:
                        descriptor = self.get_node_metadata(name)
                        nodes.append(descriptor)
                    return nodes
            
                # def get_zone_map(self):
                #     zone_map = {}
                #     d = self.configuration.get('availability_zones', {})
                #     for zone, items in d.iteritems():
                #         nodes = items.get('nodes', [])
                #         for node in nodes:
                #             if node in zone_map:
                #                 raise Exception('failed to add node[%s] to availability zone[%s], it has already been added!'
                #                                 % node, zone)
                #             zone_map[node] = items.get('subnet_id')
                #     return zone_map
            
                def _get_service_nodes(self, config_type):
                    services = {}
                    for service, configuration in self.configuration[config_type].iteritems():
                        for node in configuration["nodes"]:
                            if node not in services:
                                services[node] = {}
                            services[node][service] = configuration.get("instances", 1)
            
                    result = [(self.get_node_metadata(node), services[node]) for node in services.iterkeys()]
                    return result
            
                def update_env_nodes(self, node_dict):
                    
                    self.configuration['definition']['environment']['nodes'] = {}
                    self.configuration['definition']['environment']['nodes'].update(node_dict)
            
                def get_private_key(self):
                    
                    try:
                        return self.configuration['definition']['environment']['key_file_path']
                    except KeyError:
                        raise click.UsageError(click.style("Your config is missing the SSH key path. Please run the privatekey command to set it", fg='red', bold=True))
            
            
                def set_private_key(self, key_file_path):
                    
                    self.configuration['definition']['environment']['key_file_path'] = key_file_path
                    self.save_config()
            
                def save_config(self, config_filename='config.json'):
                    with open (config_filename, 'w') as config_file:
                        json.dump(self.configuration, config_file, sort_keys=True, indent=4)
            
                def get_elk_host(self):
                    return self.configuration['definition']['elk_host']
            
                def get_elk_port(self):
                    return self.configuration['definition']['elk_port']
            
                def get_SSH_username(self):
                    return self.configuration['definition']['SSH_username']
            
                def get_SSH_password(self):
                    return self.configuration['definition']['SSH_password']"""